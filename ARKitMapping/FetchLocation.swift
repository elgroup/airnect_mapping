//
//  CoreLocation.swift
//  AirnectFloorMapping
//
//  Created by Anvesh on 07/08/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

protocol ShowAddressDelegate: class {
    func setAddress(placemark:CLPlacemark, latitude:Double, longitude:Double)
}


class FetchLocation:NSObject,CLLocationManagerDelegate{
    
    weak var delegate:ShowAddressDelegate?
    var locationManager = CLLocationManager()
    
    override init(){
        super.init()
        self.isAuthorizedtoGetUserLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            //do whatever init activities here.
        }
    }
    
    
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationManager.stopUpdatingLocation()
       // self.getAddressFromLatLon(pdblLatitude: 40.6892, withLongitude: -74.0445)
        self.getAddressFromLatLon(pdblLatitude:locValue.latitude, withLongitude: locValue.longitude)
        
        //store the user location here to firebase or somewhere
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude //Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = pdblLongitude//Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                    
                    //let address:AddressTableViewController = AddressTableViewController()
                    //address.setAddress(placemark: pm)
                    //self.setAddress(placemark: pm, address: address)
                    self.delegate?.setAddress(placemark: pm, latitude: lat, longitude: lon)
                }
        })
    }
    
//    func coordinateToAddress(_ coordinate: CLLocationCoordinate2D) -> Void {
//        locationSelected = CLLocation.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
//        GMSGeocoder.init().reverseGeocodeCoordinate(coordinate) { (response, error) in
//            if response != nil && ((response?.results()) != nil) {
//                var addressStr: String = ""
//                for address:GMSAddress in (response?.results())! {
//                    let a = Double((address.lines?.count)!)
//                    let b = Double(0)
//                    if a > b {
//                        addressStr = (address.lines?.joined(separator: ", "))!
//                    }
//                    break
//                }
//                DispatchQueue.main.async {
//                    self.strAddress = addressStr
//                    self.tfAddress.text = addressStr
//                }
//            }
//        }
//    }

}
