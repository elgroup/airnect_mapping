//
//  Graph.swift
//  ARKitMapping
//
//  Created by Anvesh on 11/06/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import Foundation
import UIKit
import SceneKit
import ARKit

public class Graph: NSObject{
    var appDelegate:AppDelegate!
    var arrVisitedNodes:NSMutableArray!
    var arrUnvistedNodes: NSMutableArray!
    var arrDistance:NSMutableArray!
    var array1:NSMutableArray!
    var isFirst = true
    
    var arrNodes: Array<Node> = Array<Node>()
    var graph: Array<Edge> = Array<Edge>()
    var spanningTree: Array<SpanningNode> = Array<SpanningNode>()
    var sourceNode: Node?
    var destinationNode: Node?
    var visitedEdges: Array<Edge> = Array<Edge>()
    var sortestPathEdges: Array<Edge> = Array<Edge>()
    var shortestPath: Array<Node> = Array<Node>()
    
    
    public override init() {
        appDelegate =  UIApplication.shared.delegate as! AppDelegate
        arrVisitedNodes = NSMutableArray()
        arrUnvistedNodes = NSMutableArray()
        arrDistance = NSMutableArray()
        array1 = NSMutableArray()
    }
    
    func checkNearestNode(sceneView:ARSCNView, id:Int){
        // let viewCurrent:ViewController = ViewController()
        let x:Float = UserDefaults.standard.float(forKey: "x")
        let y:Float = UserDefaults.standard.float(forKey: "y")
        let z:Float = UserDefaults.standard.float(forKey: "z")
        
        let currentPosition: SCNVector3 = SCNVector3(x,y,z)
//        let vectorCurrent:Vector = Vector()
//        vectorCurrent.x = currentPosition.x
//        vectorCurrent.y = currentPosition.y
//        vectorCurrent.z = currentPosition.z
        
        for n in 0..<appDelegate.arrayPoints.count{//appDelegate.vectorPointArray.arrVectorPoints.count{
            var dictPoints:NSDictionary = NSDictionary.init()
            dictPoints = appDelegate.arrayPoints[n] as! NSDictionary
            let vector: SCNVector3 = SCNVector3Make(dictPoints["x"] as! Float, dictPoints["y"] as! Float, dictPoints["z"] as! Float) //appDelegate.vectorPointArray.arrVectorPoints[n].vector
            let id: Int = dictPoints["id"] as! Int
            let distance: Float = CommonMethods.sharedInstance.distanceBetweenTwoVectorsUsingSCNVector(vectorPointsFrom: vector, vectorPointsTo: currentPosition)
            let dict:NSMutableDictionary = NSMutableDictionary()
            dict.setObject(id, forKey:"id" as NSCopying)
            dict.setObject(distance, forKey: "distance" as NSCopying)
            dict.setObject(vector.x, forKey: "x" as NSCopying)
            dict.setObject(vector.y, forKey: "y" as NSCopying)
            dict.setObject(vector.z, forKey: "z" as NSCopying)


            //dict.setObject(distance as Float, forKey: "distance" as NSCopying)
            arrDistance.add(dict)
        }
        
        let sortedByDistance = arrDistance.sortedArray(using: [NSSortDescriptor(key: "distance.floatValue", ascending: true)])
        for n in 0..<sortedByDistance.count{
            let d:NSDictionary = sortedByDistance[n] as! NSDictionary
            print("\(String(describing: d.value(forKey: "distance")))")
        }
        
        let d: NSDictionary = sortedByDistance[0] as! NSDictionary
        sourceNode = Node()
        sourceNode?.id = d.value(forKey: "id") as! Int
        sourceNode?.weight = 0
        sourceNode?.x = d.value(forKey: "x") as? Float
        sourceNode?.y = d.value(forKey: "y") as? Float
        sourceNode?.z = d.value(forKey: "z") as? Float
        
        for n in 0..<appDelegate.arrayPoints.count{
            var vertex:NSDictionary = NSDictionary()
            vertex = appDelegate.arrayPoints[n] as! NSDictionary
            let node: Node = Node()
            node.id = vertex["id"] as! Int
            node.name = "\(String(describing: vertex["id"]))"
            node.x = vertex["x"] as? Float
            node.y = vertex["y"] as? Float
            node.z = vertex["z"] as? Float
            arrNodes.append(node)

            if id == vertex["id"] as! Int{
                destinationNode = node
            }
        }
        for n in 0..<appDelegate.arrayPoints.count{
            let vertex: NSDictionary = appDelegate.arrayPoints[n] as! NSDictionary
            self.makeGraph(vertex: vertex)
        }
        //        for n in 0..<graph.count {
        //            print("id = " + "\(graph[n].id)")
        //            print("id1 = " + "\(graph[n].id1)")
        //            print("id2 = " + "\(graph[n].id2)")
        //            print("distance = " + "\(graph[n].distance)")
        //            print("-----------------------------------------")
        //        }
        //
        //        print(graph.count)
        self.makeWeightedGraph(sourceNode!, destinationNode!, [sourceNode!], (sourceNode?.name)!,sceneView: sceneView)
    }
    
    
    func makeGraph(vertex:NSDictionary) {
        let edge: Edge = Edge()
        let arrAdjacent:NSMutableArray = vertex.value(forKey: "arrayAdjacent") as! NSMutableArray
        for n in 0..<arrAdjacent.count {
            edge.id = vertex.value(forKey: "id") as! Int
            edge.id1 = vertex.value(forKey: "id") as! Int
            let dict: NSDictionary = arrAdjacent[n] as! NSDictionary
            edge.id2 = dict["id"] as! Int
            edge.distance = dict["distance"] as! Float
            self.checkForExistingEdge(edge: edge)
            graph.append(edge)
        }
        
    }
    
    func checkForExistingEdge(edge:Edge){
        for n in 0..<graph.count {
            let edge1:Edge = graph[n]
            if edge.id == edge1.id && edge.id1 == edge1.id1 && edge.id2 == edge1.id2 && edge.distance == edge1.distance{
                graph.remove(at: n)
            }
            
        }
    }
    
    func makeWeightedGraph(_ fromNode: Node, _ toNode:  Node, _ arr: Array<Node>, _ str: String, sceneView:ARSCNView) -> Void {
        
        let filteredEdgesForToNode = self.graph.filter{$0.id1 == toNode.id || $0.id2 == toNode.id}
        if filteredEdgesForToNode.count == 0 {
            let viewController = ViewController()
           // print("No connected path available")
            let alert = UIAlertController.init(title: "Alert", message: "No connected path available", preferredStyle: .alert)
            let Done: UIAlertAction = UIAlertAction.init(title:"Ok" , style: .cancel, handler: nil)
            alert.addAction(Done)
            viewController.present(alert, animated: true, completion: nil)
            return
        }
        
        if fromNode.id == toNode.id {
            shortestPath = arr
            print(str + " = \(arr.count)")
            return
        }
        
        let filteredEdges = self.graph.filter{$0.id1 == fromNode.id || $0.id2 == fromNode.id}
        for filteredEdge in filteredEdges {
            if fromNode.id == sourceNode?.id {
                fromNode.weight = 0
            }
            
            var searchID = -1
            if filteredEdge.id1 != fromNode.id {
                searchID = filteredEdge.id1
            }
                
            else if filteredEdge.id2 != fromNode.id{
                searchID = filteredEdge.id2
            }
            if searchID != -1 {
                let filteredNodes = self.arrNodes.filter{$0.id == searchID}
                for filteredNode in filteredNodes {
                    if filteredNode.id != sourceNode?.id {
                        if filteredNode.weight == -1 || filteredNode.weight > fromNode.weight + filteredEdge.distance {
                            filteredNode.weight = fromNode.weight + filteredEdge.distance
                            print(fromNode.name + " - " + filteredNode.name + " - " + toNode.name)
                            var foundPathArray: Array<Node> = arr
                            foundPathArray.append(filteredNode)
                            self.makeWeightedGraph(filteredNode, toNode, foundPathArray, str+filteredNode.name,sceneView: sceneView)
                        }
                    }
                }
            }
        }
        let viewController:ViewController = ViewController()
        viewController.makePath(arr: shortestPath,sceneView: sceneView)
        
        let spanningNode: SpanningNode = SpanningNode()
        spanningNode.fromNodeID = -1
        spanningNode.weight = 0
        
    }
}
