//
//  Edge.swift
//  ARKitMapping
//
//  Created by Anvesh on 08/06/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import Foundation

public class Edge {
    
    var neighbor: Vertex
    var weight: Int
    
    init() {
        weight = 0
        self.neighbor = Vertex()
    }
    
}
