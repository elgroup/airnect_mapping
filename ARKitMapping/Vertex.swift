//
//  File.swift
//  ARKitMapping
//
//  Created by Anvesh on 08/06/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import Foundation
import ARKit
import SceneKit

public class Vertex:NSObject{
    var id:Int!
    var vector:Vector!
    //var arrAdjacent:Array<VectorDistance> = Array<VectorDistance>()
    var arrAdjacent:NSMutableArray = NSMutableArray()
    var node:SCNNode = SCNNode()
    var isAlreadyInserted:Bool!
}

public class Vector:NSObject{
    var x:Float!
    var y:Float!
    var z:Float!
    var id: Int = 0
    
}

public class VectorDistance:NSObject{
    var vector:Vector!
    var distance: Float = 0.0
}

public class VertexArray:NSObject{
    var arrVectorPoints:Array<Vertex> = Array<Vertex>()
}

class Edge: NSObject {
    var id: Int = -1
    var id1: Int = -1
    var id2: Int = -1
    var distance: Float = 0.0
}

class Node: NSObject {
    var name: String = ""
    var id: Int = 0
    var weight: Float = -1.0
    var x:Float!
    var y:Float!
    var z:Float!
}

class SpanningNode: NSObject {
    var fromNodeID: Int = -1
    var nodeID: Int = -1
    var weight: Float = 0.0
}
