//
//  LoginViewController.swift
//  AirnectFloorMapping
//
//  Created by Anvesh on 17/08/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var imageViewLogo: UIImageView!
    var loaderView: Loader = Loader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewLogo.layer.cornerRadius = imageViewLogo.frame.size.height/2
        imageViewLogo.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Textfield Delegate and data Source
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldEmail{
            textField.becomeFirstResponder()
        }
        else if textField == textFieldPassword{
            textFieldEmail.resignFirstResponder()
            textField.becomeFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == textFieldEmail{
            textField.resignFirstResponder()
            textFieldPassword.becomeFirstResponder()
        }
        return true
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        
        
        if textFieldEmail.text?.count == 0{
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter the Library Name", Controller: self, Time: 3)
        }
        else if textFieldPassword.text?.count == 0{
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter Address", Controller: self, Time: 3)
        }
        else{
            self.sendAuthenticationForLogin()
        }
    }
    
    func sendAuthenticationForLogin(){
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            
            loaderView.showLoaderOnScreen(vc: self, view:self.view)
            var dict: [String:Any] = [String:Any]()
            dict["email"] = textFieldEmail.text
            dict["password"] = textFieldPassword.text
            dict["device_token"] = "62513472345i3"
            
            
            var dictResponse:[String:Any] = [String:Any]()
            dictResponse["user"] = dict
            print(dictResponse)
            
            APICallExecutor.postRequestForURLString(true,"/users/sign_in", [:], dictResponse) { (Done, error, Success, result, message, isActiveSession) in
                
                DispatchQueue.main.async{
                    self.loaderView.hideLoader()
                    if Success{
                        
                        let dict:NSDictionary = result as! NSDictionary
                        let dictUser:NSDictionary = dict["user"] as! NSDictionary
                        print(dictUser)
                        let auth_token:String = dictUser.value(forKey: "auth_token") as! String
                        let defaults = UserDefaults.standard
                        defaults.set(auth_token, forKey: "auth_token")
                        
                        let arrayLibraryList:NSArray = dictUser["library_list"] as! NSArray
                        let story = UIStoryboard.init(name: "Main" , bundle: nil)
                        if arrayLibraryList.count > 1{
                            let controller:SelectSectionTableViewController = story.instantiateViewController(withIdentifier: "SelectSectionTableViewController") as! SelectSectionTableViewController
                            controller.dictUser = dictUser
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                        else{
                            
                            let controller:AddressTableViewController = story.instantiateViewController(withIdentifier: "AddressTableViewController") as! AddressTableViewController
                            controller.dictUser = dictUser
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                        
                    }
                    else {
                        MyCustomClass.showAlert(Title: "Alert", Message: "You are not a valid user!!!", Controller: self, Time: 3)
                    }
                }
            }
        }
        else{
            MyCustomClass.showAlert(Title: "Alert", Message: "No internet connection", Controller: self, Time: 3)
        }
    }
    
    @IBAction func Tapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
