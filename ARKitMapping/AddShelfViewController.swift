//
//  AddShelfViewController.swift
//  AirnectFloorMapping
//
//  Created by Anvesh on 26/09/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import UIKit
import CoreData

class AddShelfViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var labelShelfCode: UILabel!
    @IBOutlet weak var labelShelfNumber: UILabel!
    @IBOutlet weak var textFieldBookNumber: UITextField!
    @IBOutlet weak var viewShelfDetail: UIView!
    var dictShelf:NSDictionary!
    var floorNumber:Int!
    var nodeId:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isSelectionOfBooks = true
        self.navigationController?.navigationBar.isHidden = false
        
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func buttonDonePressed(_ sender: Any) {
        let isInternet:Bool = Reachability.isConnectedToNetwork()
        if isInternet{
            self.view.endEditing(true)
            var dictHeader:[String:String] = [String:String]()
            dictHeader["auth_token"] = UserDefaults.standard.value(forKey: "auth_token") as? String
            var dictResponse:[String:Any] = [String:Any]()
            dictResponse["code"] = textFieldBookNumber.text
            
            APICallExecutor.postRequestForURLString(true, "/admin/get_shelf", dictHeader, dictResponse) { (Done, error, succes, result, message,isctiveSession) in
                if succes{
                    let dict:NSDictionary = result as! NSDictionary
                    self.dictShelf = NSDictionary()
                    self.dictShelf = dict["shelf"] as? NSDictionary
                    self.alertForAssigningNodeToShelf(dict: self.dictShelf)
                }
                else{
                    
                }
            }
            
        }
        else{
            
        }
    }
    
    func alertForAssigningNodeToShelf(dict:NSDictionary){
        
        let strMessage:NSString = NSString.init(format: "The node will be assign to the shelf" )
        let alert:UIAlertController = UIAlertController.init(title: "", message:strMessage as String , preferredStyle: .alert)
        let Done:UIAlertAction = UIAlertAction.init(title: "YES", style: .default) { (action:UIAlertAction) in
            self.AddingNodeToShelf()
        }
        let Cancel: UIAlertAction = UIAlertAction.init(title: "CANCEL", style: .cancel) { (action:UIAlertAction) in
            
        }
        alert.addAction(Done)
        alert.addAction(Cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func CloseButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    func AddingNodeToShelf(){
        
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "ShelfMapping", in: context)
            let newUser = NSManagedObject(entity: entity!, insertInto: context)
            let shelfId:Int = self.dictShelf["shelf_id"] as! Int
            newUser.setValue(self.nodeId, forKey: "node_id")
            newUser.setValue(shelfId, forKey: "shelf_id")
            newUser.setValue(false, forKey: "is_added")
            do {
                try context.save()
            } catch {
                print("Failed saving")
            }
            
            self.navigationController?.popViewController(animated: true)
        }
        
        //        let isInternet:Bool = Reachability.isConnectedToNetwork()
        //        if isInternet {
        //        var dictHeader:[String:String] = [String:String]()
        //
        //        var dictResponse:[String:Any] = [String:Any]()
        //
        //        APICallExecutor.postRequestForURLString(true, "", dictHeader, dictResponse) { (Done, error, succes, result, message,isctiveSession) in
        //
        //        }
        //        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
