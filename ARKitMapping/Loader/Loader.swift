//
//  Loader.swift
//  BluLint
//
//  Created by hament miglani on 24/03/2016 .
//  Copyright © 2016 IRESLAB. All rights reserved.
//

import UIKit

class Loader : NSObject{
    static let sharedLoader = Loader()
    var spinnerView = SpinnerView()
    let view:UIView
    override init() {
        view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        view.addSubview(spinnerView)
    }
    
    func showLoaderOnScreen(vc:UIViewController, view:UIView){
      DispatchQueue.main.async {
        let rect = view.frame as CGRect
        //self.spinnerView.frame = CGRect(x:UIScreen.main.bounds.size.width/2 + 50, y:UIScreen.main.bounds.size.height/2 - 35, width:50, height:50)
        self.spinnerView.frame = CGRect(x:rect.origin.x + rect.size.width/2 - 25 , y: rect.origin.y + rect.size.height/2 - 25, width: 50, height: 50)
        AppDelegate.getAppDelegate().window?.addSubview(self.view)
        }
    }
    
    func hideLoader(){
        DispatchQueue.main.async {
        self.view.removeFromSuperview()
        }
    }

    
}
