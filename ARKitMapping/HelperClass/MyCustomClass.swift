                                                                                                                                         //
//  MyCustomClass.swift
//  MySwiftExample
//
//  Created by anupama on 24/01/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit
import QuartzCore
import Foundation
import CoreLocation

/////////////////////////////////////////////////////////////////
/////////////////////Importent Note////////////////////////////
//////////// //////////////////////////////////////////////////////
///1. To Remove ARC from spacific class///
///// add -:"-fno-objc-arc" in Compiler Flags///
//// how to go there:-click on target->compile Sources->choose
//// any .m file which you want to remove ARC from file//////
///@end///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
///2. To Remove ARC Related from classes having ARC////
///// add -:"-fobjc-arc-exceptions" in comiler Flags///
///// how to go there:-click on target->compile Sources->choose
//// any .m file which you want to remove ARC from file//////
///@end///////////////////////////////////////////////////////////
//MARK:GOOGLE API..............................
var GOOGLE_GECODE_URL = String(format: "https://maps.googleapis.com/maps/api/geocode/json?")
var GOOGLE_DIRECTIONS_URL = String(format: "https://maps.googleapis.com/maps/api/directions/json?")

var IS_IPHONE6plus = UIScreen.main.bounds.size.height == 736
var IS_IPHONE6 = UIScreen.main.bounds.size.height == 667
var IS_IPHONE5 = UIScreen.main.bounds.size.height == 568
var IS_IPHONE4 = UIScreen.main.bounds.size.height == 480
var CUREENT_SYSTEM_VERSION = UIDevice.current.systemVersion
let mainStoryboard = UIStoryboard()
let navigationController = UINavigationController()

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
enum URLSchemes: String {
    case http = "http://", https = "https://", ftp = "ftp://", unknown = "unknown://", www = "www."
    
    static func detectScheme(urlString: String) -> URLSchemes {
        
        if URLSchemes.isSchemeCorrect(urlString: urlString, scheme: .http) {
            return .http
        }
        if URLSchemes.isSchemeCorrect(urlString: urlString, scheme: .https) {
            return .https
        }
        if URLSchemes.isSchemeCorrect(urlString: urlString, scheme: .ftp) {
            return .ftp
        }
        if URLSchemes.isSchemeCorrect(urlString: urlString, scheme: .www) {
            return .ftp
        }
        return .unknown
    }
    
    static func getAllSchemes(separetedBy separator: String) -> String {
        return "\(URLSchemes.http.rawValue)\(separator)\(URLSchemes.https.rawValue)\(separator)\(URLSchemes.ftp.rawValue)"
    }
    
    private static func isSchemeCorrect(urlString: String, scheme: URLSchemes) -> Bool {
        if urlString.replacingOccurrences(of: scheme.rawValue, with: "") == urlString {
            return false
        }
        return true
    }
}

extension String {
    
    var isUrl: Bool {
        
        // for http://regexr.com checking
        // (?:(?:https?|ftp):\/\/)(?:xn--)?(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[#-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?
        
        let schemes = URLSchemes.getAllSchemes(separetedBy: "|").replacingOccurrences(of: "://", with: "")
        let regex = "(?:(?:\(schemes)):\\/\\/)(?:xn--)?(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\.254(?:\\.\\d{1,3}){2})(?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[#-z\\u00a1-\\uffff]{2,})))(?::\\d{2,5})?(?:\\/[^\\s]*)?"
        
        
        let regularExpression = try! NSRegularExpression(pattern: regex, options: [])
        let range = NSRange(location: 0, length: self.characters.count)
        let matches = regularExpression.matches(in: self, options: [], range: range)
        for match in matches {
            if range.location == match.range.location && range.length == match.range.length {
                return true
            }
        }
        return false
    }
    
    var toURL: URL? {
        
        let urlChecker: (String)->(URL?) = { url_string in
            if url_string.isUrl, let url = URL(string: url_string) {
                return url
            }
            return nil
        }
        
        if !contains(".") {
            return nil
        }
        
        if let url = urlChecker(self) {
            return url
        }
        
        let scheme = URLSchemes.detectScheme(urlString: self)
        if scheme == .unknown {
            let newEncodedString = URLSchemes.http.rawValue + self
            if let url = urlChecker(newEncodedString) {
                return url
            }
        }
        
        return nil
    }
}

extension String {
    func isEmptyAndContainsNoWhitespace() -> Bool {
        guard self.isEmpty, self.trimmingCharacters(in: .whitespaces).isEmpty
            else {
                return false
        }
        return true
    }
}
extension UICollectionView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .lightGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.italicSystemFont(ofSize: 18.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel
    }
    
    func restore() {
        self.backgroundView = nil
        
    }
}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .lightGray
        
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont.italicSystemFont(ofSize: 18.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .none
    }
}
//MARK: .MESSAGE SHOW REGARD API CALL............................
var k_LOAD_MSG = String(format: "Please Wait..")
var k_ERROR_MSG = String(format: "No Data Available.\n Please Try Again")
var k_SUCCESS_MSG = String(format: "Data Successfully Show")
var k_NETOFFLINE_MSG = String(format: "The Internet connection \n appears to be offline.")


//MARK: .UserDefaults Extesnion............................
extension UserDefaults {
    /// Convenience method to wrap the built-in .integer(forKey: ) method in an optional returning nil if the key doesn't exist.
    func integerOptional(forKey: String) -> Int? {
        guard self.object(forKey: forKey) != nil else { return nil }
        return self.integer(forKey: forKey)
    }
    /// Convenience method to wrap the built-in .double(forKey: ) method in an optional returning nil if the key doesn't exist.
    func doubleOptional(forKey: String) -> Double? {
        guard self.object(forKey: forKey) != nil else { return nil }
        return self.double(forKey: forKey)
    }
    /// Convenience method to wrap the built-in .float(forKey: ) method in an optional returning nil if the key doesn't exist.
    func floatOptional(forKey: String) -> Float? {
        guard self.object(forKey: forKey) != nil else { return nil }
        return self.float(forKey: forKey)
    }
    /// Convenience method to wrap the built-in .bool(forKey: ) method in an optional returning nil if the key doesn't exist.
    func boolOptional(forKey: String) -> Bool? {
        guard self.object(forKey: forKey) != nil else { return nil }
        return self.bool(forKey: forKey)
    }
}

//MARK: .Background Dispatch method............................

extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
    ////...used ad method;;
    //**1  DispatchQueue.background(delay: 3.0, background: {
    // do something in background
    //}, completion: {
    // when background job finishes, wait 3 seconds and do something in main thread
    //})
    
    //2.DispatchQueue.background(background: {
    // do something in background
    // }, completion:{
    // when background job finished, do something in main thread
    // })
    
    //3. DispatchQueue.background(delay: 3.0, completion:{
    // do something in main thread after 3 seconds
    //})
    //**
    
}

//MARK: .UIAlertController Extension...........................
extension UIAlertController {
    static func notifyUser(_ title: String, message: String, alertButtonTitles: [String], alertButtonStyles: [UIAlertActionStyle], vc: UIViewController, completion: @escaping (Int)->Void) -> Void
    {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        for title in alertButtonTitles {
            let actionObj = UIAlertAction(title: title,
                                          style: alertButtonStyles[alertButtonTitles.index(of: title)!], handler: { action in
                                            completion(alertButtonTitles.index(of: action.title!)!)
            })
            
            alert.addAction(actionObj)
        }
        
        
        //vc will be the view controller on which you will present your alert as you cannot use self because this method is static.
        vc.present(alert, animated: true, completion: nil)
    }
}
//MARK:....................1..................................................
//MARK: App Launch first time  in Iphone
private var firstLaunch : Bool = false

extension UIApplication {
    
    static func isFirstLaunch() -> Bool {
        let firstLaunchFlag = "isFirstLaunchFlag"
        let isFirstLaunch = UserDefaults.standard.string(forKey: firstLaunchFlag) == nil
        if (isFirstLaunch) {
            firstLaunch = isFirstLaunch
            UserDefaults.standard.set("false", forKey: firstLaunchFlag)
            UserDefaults.standard.synchronize()
        }
        return firstLaunch || isFirstLaunch
    }
}

class MyCustomClass: NSObject
{
    
    //MARK:....................1..................................................
    //MARK: Save Data in Dictionary after getting JSON response
    static func jsonSerializationDictionary(responseData: Data?) -> [String: Any]
    {
        guard let dataVal = responseData else { return [:]  }
        if let JSONObject  = try? JSONSerialization.jsonObject(with: dataVal, options: .mutableContainers)
        {
            print("elim is an array")
            return (JSONObject as? [String: Any])!
        }
        
        return [:]
    }
    //MARK:....................1..................................................
    //MARK:  DispatchQueue in Background....Method.........
    func backgorundFunction()  {
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
            }
        }
    }
    //MARK:...........valid url with or without http ,https ,www.................................................
    //MARK:  Optional Check Valid URL...Method.........
    static func isOptionalValidUrl(url: String) -> Bool {
        let urlRegEx = "(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/?)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))*(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’])*)"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: url)
        return result
    }
    func UTCToLocal(UTCDateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Input Format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let UTCDate = dateFormatter.date(from: UTCDateString)
        dateFormatter.dateFormat = "yyyy-MMM-dd hh:mm:ss" // Output Format
        dateFormatter.timeZone = TimeZone.current
        let UTCToCurrentFormat = dateFormatter.string(from: UTCDate!)
        return UTCToCurrentFormat
    }
    func localToUTC(date:String, fromFormat: String, toFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.date
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    //MARK:....................1..................................................
    //MARK:  DispatchQueue in Main After some Time....Method.........
    static func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    static func convertCLLocationDistanceToMiles ( targetDistance : CLLocationDistance?) -> CLLocationDistance {
        var targetDistance = targetDistance
        targetDistance =  targetDistance!*0.00062137
        return targetDistance!
    }
    //MARK:...................2...................................................
    //MARK:distanceBetweenTwoLocations in KiloMeters
    static func convertCLLocationDistanceToKiloMeters ( targetDistance : CLLocationDistance?) -> CLLocationDistance {
        var targetDistance = targetDistance
        targetDistance =  targetDistance!/1000
        return targetDistance!
    }
    func dateFormatGet(myDate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: myDate)!
        dateFormatter.dateFormat = "MMM dd ,yyyy"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    //MARK:...................2...................................................
    //MARK:distanceBetweenTwoLocations in KiloMeters
    static func convertCLLocationDistanceToKiloMeters (myLat:Double,myLong:Double,targetLat:Double,targetLong:Double) -> String {
        let myLocation = CLLocation(latitude: myLat, longitude:myLong)
        let targetLocation = CLLocation(latitude: targetLat, longitude: targetLong)
        let distanceMeters = myLocation.distance(from: targetLocation)
        let distanceKM = distanceMeters / 1000
        let roundedTwoDigit =  String(format: "%.2f", distanceKM)
        return roundedTwoDigit
    }
    
    //MARK:...................2...................................................
    //MARK:distanceBetweenTwoLocations in Meters
    static func convertCLLocationDistanceToMeters(myLat:Double,myLong:Double,targetLat:Double,targetLong:Double) -> String{
        
        let myLocation = CLLocation(latitude: myLat, longitude:myLong)
        let targetLocation = CLLocation(latitude: targetLat, longitude: targetLong)
        let distanceMeters = myLocation.distance(from: targetLocation)
        let roundedTwoDigit =  String(format: "%f", distanceMeters)
        return roundedTwoDigit
        
    }
    
    //MARK:...................2...................................................
    //MARK:distanceBetweenTwoLocations in MILES
    static func distanceBetweenTwoLocations(myLat:Double,myLong:Double,targetLat:Double,targetLong:Double) -> String{
        
        let myLocation = CLLocation(latitude: myLat, longitude:myLong)
        let targetLocation = CLLocation(latitude: targetLat, longitude: targetLong)
        let distanceMeters = myLocation.distance(from: targetLocation)
        let distanceKM = distanceMeters / 1000
        let distanceInMiles = distanceKM * 0.62137
        let roundedTwoDigit =  String(format: "%.2f", distanceInMiles)
        return roundedTwoDigit
        
    }
    //MARK:-- searchBtnPressed Method...........................
    @IBAction func searchBtnPressed(_ sender: AnyObject) {
        
        guard let button = sender as? UIButton else { return }
        
        if !button.isSelected {
            button.isSelected = true
            button.setTitleColor(UIColor.blue, for: UIControlState.normal)
            button.backgroundColor = UIColor.green
            
            print("selcted")
        }
        else {
            button.isSelected = false
            button.setTitleColor(UIColor.green, for: UIControlState.normal)
            button.backgroundColor = UIColor.blue
            print("againselcted")
        }
    }
    
    //MARK:...................2...................................................
    //MARK:...isValidArray
    static func isValidArray(myArray :[AnyObject]) -> Bool
    {
        
        if myArray.isEmpty || myArray.count == 0 || myArray.count < 0 || myArray.isEmpty == true
        {
            return false
        }
        else
        {
            return true
        }
        
        
    }
    //MARK:...................2...................................................
    //MARK:...isValidDictionary
    static func isValidDictionary(myDict :[String:AnyObject]) -> Bool
    {
        
        if myDict.isEmpty || myDict.count == 0 || myDict.count < 0 || myDict.isEmpty == true
        {
            return false
        }
        else
        {
            return true
        }
        
        
    }
    //MARK:...................2...................................................
    //MARK:Save Data in Array after getting JSON response
    static func jsonSerializationArray(responseData: Data?) -> [AnyObject]
    {
        guard let dataVal = responseData else { return []  }
        
        if let elim : Any = try? JSONSerialization.jsonObject(with: dataVal, options: .mutableContainers)
        {
            print("elim is an array")
            return (elim as AnyObject) as! [AnyObject]
        }
        return []
    }
    //MARK:......................3................................................
    //MARK: Try and throws.. Convert to  Dictionary after getting string Dictionary Json
    static func stringDictionaryJsonConvertToDictionary(from text: String) throws -> [String: String]
    {
        guard let data = text.data(using: .utf8) else { return [:] }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: String] ?? [:]
    }
    //MARK:......................4................................................
    //MARK: Try and throws.. Convert to  Array after getting string Array Json
    static func stringArrayJsonConvertToArray(from text: String) throws -> [String]
    {
        guard let data = text.data(using: .utf8) else { return [] }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String] ?? []
    }
    //MARK:.......................5...............................................
    //MARK: Try and throws.. Convert to  String after getting string Json
    static func stringJsonConvertToString(from text: String) throws -> String
    {
        guard let data = text.data(using: .utf8) else { return "Empty String" }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? String ?? "Not Convert into String"
    }
    //MARK:......................6................................................
    //MARK:  Convert to  Dictionary after getting JSON response
    static func jsonStringDictionaryconvertToStringDictionary(from response: String) -> [String: String] {
        guard let data = response.data(using: .utf8) else { return [:] }
        let anyResult: Any? = try? JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: String] ?? [:]
    }
    //MARK:.....................7.................................................
    //MARK: Convert to  Array after getting JSON response
    static func jsonStringArrayconvertToStringArray(from response: String) -> [String] {
        guard let data = response.data(using: .utf8) else { return [] }
        let anyResult: Any? = try? JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String] ?? []
    }
    //MARK:.....................8.................................................
    //MARK: Convert to  String after getting JSON response
    static func jsonStringConvertToString(from response: String) -> String {
        guard let data = response.data(using: .utf8) else { return "Empty String" }
        let anyResult: Any? = try? JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? String ?? "Not convert into string"
    }
    //MARK:......................9................................................
    //MARK:Check Json in Array,Dictionary or String after getting JSON response
    
    static func checkJsonArrayDictionaryString(data: Data?) -> AnyObject
    {
        guard let dataVal = data else { return "nil" as AnyObject }
        if let elim : Any = try? JSONSerialization.jsonObject(with: dataVal, options: .mutableContainers)
        {
            print("elim is a dictionary")
            return elim as AnyObject
            
        } else if let elim : Any = try? JSONSerialization.jsonObject(with: dataVal, options: .mutableContainers)
        {
            print("elim is an array")
            return elim as AnyObject
        }
        else if let elim : Any = try? JSONSerialization.jsonObject(with: dataVal, options: .mutableContainers)
        {
            print("elim is an string")
            
            return elim as AnyObject
        }
        else if (dataVal.isEmpty)
        {
            print(dataVal )
            return dataVal as AnyObject 
        }
            
        else
        {
            print("dataVal is not valid JSON data")
        }
        return dataVal as AnyObject 
    }
    
    //MARK:......................10................................................
    //MARK: Set Any View Color By Passing hexadecimal String.....................
    static func colorWithHexString (hexString:String) -> UIColor
    {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count
        {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        
        return  UIColor.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    //MARK:......................43.............................
    //MARK:  Identify Device or Simluator Method...................
    static  func IS_SIMULATOR() -> Bool
    {
        var DEVICE_IS_SIMULATOR:Bool
        #if arch(i386) || arch(x86_64)
            DEVICE_IS_SIMULATOR = true
        #else
            DEVICE_IS_SIMULATOR = false
        #endif
        return DEVICE_IS_SIMULATOR
        
    }
    //MARK:...................11...................................................
    //MARK:Url white space replace by %20 in string ..................
    static func whiteSpaceReplaceFromString (insertString:String) -> String
    {
        let urlString = insertString.replacingOccurrences(of:" ", with: "%20")
        return urlString
    }
    
    //MARK:....................12..................................................
    //MARK:Url Email Validation  .............................................
    static func validateEmail (email:String)-> Bool
    {
        do
        {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: email, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, email.characters.count)) != nil
        }
        catch
        {
            return false
        }
        
        
    }
    //MARK:....................13..................................................
    //MARK:.......... Email Validation  .............................................
    static func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    //MARK:..................13....................................................
    //MARK:Pincode Validation  .............................................
    static func ValidPincode(value: String) -> Bool
    {
        if value.characters.count == 6
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:..................14....................................................
    //MARK:Current Password and Confirm Password same Validation  .............................................
    static func newandConfirmPasswordValidation(newpassword: String , confirmPassword : String) -> Bool
    {
        if newpassword == confirmPassword
        {
            return true
        }
        else
        {
            return false
        }
    }
    //MARK:..................15....................................................
    //MARK:Current Password and Confirm Password Length Validation  .............................................
    static func passwordLengthValidation(password: String , confirmPassword : String , lengthSize:Int ) -> Bool
    {
        if password.characters.count <= lengthSize && confirmPassword.characters.count <= lengthSize
        {
            return true
        }
        else{
            return false
        }
    }
    
    //MARK:..................16....................................................
    //MARK: Mobile number Validation .............................................
    static  func validatePhoneNumber (phoneNumbervalue: String) -> Bool
    {
        let PHONE_REGEX = "[0-9]{10}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if  phoneTest.evaluate(with: phoneNumbervalue) == true
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:...................17...................................................
    //MARK: Mobile number Numeric Validation .............................................
    static func phoneNumberNumericValidation (phoneNumber: String) -> Bool
    {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        if phoneNumber == filtered
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:.................18.....................................................
    //MARK: Age Validation .............................................
    static func ageValidation (ageValue: String) -> Bool
    {
        let PHONE_REGEX = "[0-9]{2}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if  phoneTest.evaluate(with: ageValue) == true
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:................19......................................................
    //MARK: Blank String OR Textfield Validation .............................................
    static func blankStringValidation (blankString:String)-> Bool
    {
        let trimmed =  blankString.trimmingCharacters(in: .whitespaces)
        if trimmed.isEmpty
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:.................20.....................................................
    //MARK: Password Validation with Minimum range and Maximum range.............................................
    static func passwordValidationWithExpersionAndRange (password:String, minimumLength:Int, maximumLength:Int)-> Bool
    {
        do
        {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in:password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, password.characters.count)) != nil)
            {
                if(password.characters.count>=minimumLength && password.characters.count<=maximumLength)
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false
            }
        }
        catch
        {
            return false
        }
    }
    //MARK:..................21....................................................
    //MARK:Check String blank or not .............................................
    static func isBlank (optionalString :String?) -> Bool {
        if let string = optionalString {
            return string.isEmpty
        } else {
            return true
        }
    }
    //MARK:..................22....................................................
    //MARK: Dictionary Convert into JsonString............................................
    static func dictionaryConvertIntoJsonString (jsonObjectDictionary: [String: Any]) -> String
    {
        var jsonString :String! = nil
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObjectDictionary, options: .prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            return jsonString
        }
        catch
        {
            print(error.localizedDescription)
        }
        return jsonString
    }
    
    //MARK:..................23....................................................
    //MARK: 1.Dictionary Convert into JsonData............................................
    static func dictionaryConvertIntoJsonData (jsonObjectDictionary: [String: Any]) -> Data
    {
        let jsonData :Data! = nil
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObjectDictionary, options: .prettyPrinted)
            return jsonData
        }
        catch
        {
            print(error.localizedDescription)
        }
        return jsonData
    }
    
    //MARK:.................24.....................................................
    //MARK: Array Convert into JsonData............................................
    static func arrayConvertIntoJsonData (jsonObjectArray: [Any]) -> Data
    {
        var jsonData :Data! = nil
        do
        {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObjectArray, options: .prettyPrinted)
            return jsonData
            
        }
        catch
        {
            print(error.localizedDescription)
        }
        return jsonData
    }
    
    //MARK:...............25.......................................................
    //MARK: Array Convert into JsonString............................................
    static func arrayConvertIntoJsonString (jsonObjectArray: [Any]) -> String
    {
        var jsonString :String! = nil
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObjectArray, options: .prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            return jsonString
        }
        catch
        {
            print(error.localizedDescription)
        }
        return jsonString
    }
    //MARK:.....................26.................................................
    //MARK: Phone calling............................................
    static func phoneCall(phoneNumber: String) {
        let cleanNumber = phoneNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        guard let number = URL(string: "telprompt://" + cleanNumber) else { return }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    //MARK:....................27..................................................
    //MARK: Open Url from App............................................
    static func openUrl(from stringUrl:String)
    {
        let url = URL(string: stringUrl)
        if UIApplication.shared.canOpenURL(url!) {
            //If you want handle the completion block than
            UIApplication.shared.open(url!, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
    }
    //MARK:....................28..................................................
    //MARK: Different color provide to Single String............................................
    static func createAttributedString(fullString: String, fullStringColor: UIColor, subString: String, subStringColor: UIColor) -> NSMutableAttributedString
    {
        let range = (fullString as NSString).range(of: subString)
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: fullStringColor, range: NSRange(location: 0, length: fullString.characters.count))
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: subStringColor, range: range)
        return attributedString
    }
    //MARK:.....................29.................................................
    //MARK: Status Bar Background Color Change
    static func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    //MARK:......................30................................................
    //MARK: ImageView make Circle.......................
    static func circleMakeImageView(from photoImageView: UIImageView,borderColor:UIColor,borderWidth:CGFloat) {
        
        photoImageView.layer.borderColor = borderColor.cgColor
        photoImageView.layer.borderWidth = borderWidth
        photoImageView.layer.cornerRadius = photoImageView.frame.size.width/2
        photoImageView.clipsToBounds = true
        
    }
    //MARK:......................31................................................
    //MARK: Make Rounded Button.......................
    static func roundedButton(from startedBtn:UIButton,width:CGFloat,height:CGFloat){
        let maskPAth = UIBezierPath(roundedRect: startedBtn.bounds,
                                    byRoundingCorners: [.topLeft , .topRight , .bottomLeft , .bottomRight],
                                    cornerRadii:CGSize(width:width, height:height))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = startedBtn.bounds
        maskLayer.path = maskPAth.cgPath
        startedBtn.layer.mask = maskLayer
    }
    //MARK:.....................32.................................................
    //MARK: Make Rounded Label.......................
    static func roundedLabel(from startedLabel:UILabel,width:CGFloat,height:CGFloat){
        let maskPAth = UIBezierPath(roundedRect: startedLabel.bounds,
                                    byRoundingCorners: [.topLeft , .topRight , .bottomLeft , .bottomRight],
                                    cornerRadii:CGSize(width:width, height:height))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = startedLabel.bounds
        maskLayer.path = maskPAth.cgPath
        startedLabel.layer.mask = maskLayer
    }
    //MARK:Ruby on Rail.............33.........................
    //MARK:PNGImage Convert Into Base 64................................
    static func PNGImageConvertIntoBase64(From myimage:UIImage) -> String
    {
        //let userImage:UIImage = UIImage(named:ImageNamed)!
        let imageData:Data = UIImagePNGRepresentation(myimage)!
        let base64String = imageData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        let base64StringImage = String(format: "data:image/png;base64,%@",base64String)
        return base64StringImage
    }
    //MARK:Ruby on Rail..............34.........................
    //MARK:JPGImage Convert Into Base 64................................
    static func JPGImageConvertIntoBase64(From ImageNamed:String) -> String
    {
        let userImage:UIImage = UIImage(named:ImageNamed)!
        let imageData:Data = UIImageJPEGRepresentation(userImage, 1.0)!
        let base64String = imageData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        let base64StringImage = String(format: "data:image/jpeg;base64,%@",base64String)
        return base64StringImage
    }
    //MARK:......................35.............................
    //MARK:Textfield Placeholder Text color and Font...................
    //    static func TextfieldPlaceHolderTextColor(From textField:UITextField,Text textNamed:String,ForGroundColor ColorFor:UIColor,BackGroundColor ColorBack:UIColor,Font fontNamed:String,Size fontSize:CGFloat,UnderLineStyle style:NSUnderlineStyle)
    //    {
    //        var multipleAttributes = [String : Any]()
    //        multipleAttributes[NSForegroundColorAttributeName] = ColorFor
    //        multipleAttributes[NSBackgroundColorAttributeName] = ColorBack
    //        multipleAttributes[NSFontAttributeName] = UIFont(name: fontNamed, size: fontSize)!
    //        multipleAttributes[NSUnderlineStyleAttributeName] = style.rawValue
    //        textField.attributedPlaceholder = NSAttributedString(string: textNamed,
    //                                                            attributes: multipleAttributes)
    //    }
    //MARK:.......................36..........................................
    //MARK:Create Dictionary With Key Array and Value array...................
    static func CreateDictioanry(SetKeyArray keyArray:[String], SetValueArray valueArray:[Any]) -> [String : Any]
    {
        var multipleAttributes = [String : Any]()
        for i in 0..<keyArray.count
        {
            multipleAttributes[keyArray[i]] = valueArray[i]
        }
        return multipleAttributes
    }
    //MARK:......................37...................................
    //MARK:Create Dictionary With Key  and Value...................
    static func CreateDictioanry(SetKey key:String, SetValue value:Any) -> [String : Any]
    {
        var multipleAttributes = [String : Any]()
        multipleAttributes[key] = value
        return multipleAttributes
    }
    static func removeNullsFromDictionary(origin:[String:AnyObject]) -> [String:AnyObject] {
        var destination:[String:AnyObject] = [:]
        for key in origin.keys {
            if origin[key] != nil && !(origin[key] is NSNull){
                if origin[key] is [String:AnyObject] {
                    destination[key] = self.removeNullsFromDictionary(origin: origin[key] as! [String:AnyObject]) as AnyObject
                } else if origin[key] is [AnyObject] {
                    let orgArray = origin[key] as! [AnyObject]
                    var destArray: [AnyObject] = []
                    for item in orgArray {
                        if item is [String:AnyObject] {
                            destArray.append(self.removeNullsFromDictionary(origin: item as! [String:AnyObject]) as AnyObject)
                        } else {
                            destArray.append(item)
                        }
                    }
                } else {
                    destination[key] = origin[key]
                }
            } else {
                destination[key] = "" as AnyObject
            }
        }
        return destination
    }
    
    
    //MARK:......................38.......................................
    //MARK:Remove Null Value From Dictionary...................
    static func removeNullValueFromDict(From Dictionary:[String: Any]?) -> [String: Any]?
    {
        guard var myDictionary = Dictionary, (Dictionary?.count)! > 0 && (Dictionary?.isEmpty) == false
            else {
                return nil
        }
        for Key in (myDictionary.keys)
        {
            if let object = myDictionary[Key] as AnyObject?
            {
                myDictionary.updateValue(object, forKey: Key)
            }
            if (myDictionary[Key] as AnyObject?)?.classForCoder == NSNull.classForCoder()
            {
                myDictionary.updateValue("", forKey: Key)
            }
            if (myDictionary[Key] as AnyObject?) is NSNull
            {
                myDictionary.updateValue("", forKey: Key)
            }
            if (myDictionary[Key] as AnyObject?) == nil
            {
                myDictionary.updateValue("", forKey: Key)
            }
            
            if let object = (myDictionary[Key] as AnyObject?) as? String, object == "NULL" || object == "null" || object == "<null>" || object == "<NULL>" || object == "<NIL>" || object == "<nil>" || object == "nil" || object == "NIL"
            {
                myDictionary.updateValue("", forKey: Key)
            }
        }
        return myDictionary
        
    }
    //MARK:......................39..........................
    //MARK:Remove Null Value From Array...................
    static func  removeNullFromArray(From jsonArray:[Any]?) -> [Any]?
    {
        guard var myArray = jsonArray, (jsonArray?.count)! > 0 && (jsonArray?.isEmpty)! == false
            else {
                return nil
        }
        for (index, value) in (myArray.enumerated())
        {
            print("\(index) = \(value)")
            if var object = (myArray[index] as AnyObject?) as? String, object == "<null>" || object == "<NULL>" || object == "NULL" || object == "null" || object == "<NIL>" || object == "<nil>"  || object == "nil" || object == "NIL"
            {
                object = ""
                myArray[index] = object
            }
            if (myArray[index] as AnyObject?)?.classForCoder == NSNull.classForCoder()
            {
                myArray[index] = ""
            }
        }
        return myArray
    }
    //MARK:......................40..................................
    //MARK:Remove Null Value From Dictionary In Array...................
    static func removeNullDictFromArray(arrayset:[Any]?) -> [Any]?
    {
        guard var nullArray = arrayset, (arrayset?.count)! > 0 && (arrayset?.isEmpty)! == false
            else
        {
            return nil
        }
        
        for (index, value) in (nullArray.enumerated())
        {
            print("\(index) = \(value)")
            
            guard var myDictionary  = (nullArray[index] as AnyObject?) as? [String:Any] , (myDictionary.count) > 0 && (myDictionary.isEmpty) == false
                else
            {
                return nil
            }
            
            for Key in (myDictionary.keys)
            {
                if let object = myDictionary[Key] as AnyObject?
                {
                    myDictionary.updateValue(object, forKey: Key)
                }
                if (myDictionary[Key] as AnyObject?)?.classForCoder == NSNull.classForCoder()
                {
                    myDictionary.updateValue("", forKey: Key)
                }
                if let object = (myDictionary[Key] as AnyObject?) as? String, object == "NULL" || object == "null" || object == "<null>" || object == "<NULL>" || object == "<NIL>" || object == "<nil>" || object == "nil" || object == "NIL"
                {
                    myDictionary.updateValue("", forKey: Key)
                }
            }
            nullArray = ([myDictionary] as [Any]?)!
            
        }
        return nullArray
    }
    //MARK:....................Call Api Method message show...............................
    //MARK:.....loadMsg Show ...................
    static func loadMsg() -> String {
        let msgString  =  k_LOAD_MSG
        return msgString
    }
    //MARK:......................41................................
    //MARK:.....errorMsg Show ...................
    static func errorMsg() -> String {
        let msgString =  k_ERROR_MSG
        return msgString
    }
    //MARK:......................41................................
    //MARK:.....successMsg Show ...................
    static func successMsg() -> String {
        let msgString =  k_SUCCESS_MSG
        return msgString
    }
    //MARK:......................41................................
    //MARK:.....Internat Connection Offline ...................
    static func netOfflineMsg() -> String {
        let msgString =  k_NETOFFLINE_MSG
        return msgString
    }
    //MARK:......................41................................
    //MARK:Check key Exist or not in USERDEFAULTS...................
    static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    //MARK:......................41................................
    //MARK:Dict Save In UserDefult...................
    static func dictSaveInUserDefult(dict:[String : Any],keyName:String)
    {
        let userDefaults: UserDefaults = UserDefaults.standard
        let watchlistData = NSKeyedArchiver.archivedData(withRootObject: dict)
        userDefaults.set(watchlistData, forKey: keyName)
        userDefaults.synchronize()
    }
    //MARK:......................42.....................
    //MARK:Dict Get From UserDefult...................
    static func dictGetFromUserDefult(keyName:String) -> AnyObject
    {
        let userDefaults: UserDefaults = UserDefaults.standard
        let watchlistData = userDefaults.object(forKey: keyName)
        
        if let watchlist : AnyObject = NSKeyedUnarchiver.unarchiveObject(with: watchlistData as! Data) as AnyObject?
        {
            // print(watchlist)
            return watchlist
        }
        return (0 as AnyObject) 
        
    }
    //MARK:......................43.............................
    //MARK:Array Save In UserDefult...................
    static  func arraySaveInUserDefult(Array array:[Any],keyName:String)
    {
        let userDefaults: UserDefaults = UserDefaults.standard
        let watchlistData = NSKeyedArchiver.archivedData(withRootObject: array)
        userDefaults.set(watchlistData, forKey: keyName)
        userDefaults.synchronize()
    }
    //MARK:......................44............................
    //MARK:Array Get From UserDefult...................
    static func arrayGetFromUserDefult(keyName:String) -> AnyObject
    {
        let userDefaults: UserDefaults = UserDefaults.standard
        let watchlistData = userDefaults.object(forKey: keyName)
        
        if let watchlist : AnyObject = NSKeyedUnarchiver.unarchiveObject(with: watchlistData as! Data) as AnyObject?
        {
            // print(watchlist)
            return watchlist
        }
        return 0 as AnyObject
        
    }
    //MARK:......................45.............................
    //MARK:String Save In  UserDefult...................
    static func stringSaveInUserDefult(dict:String,keyName:String)
    {
        let userDefaults: UserDefaults = UserDefaults.standard
        let watchlistData = NSKeyedArchiver.archivedData(withRootObject: dict)
        userDefaults.set(watchlistData, forKey: keyName)
        userDefaults.synchronize()
    }
    //MARK:......................46...........................
    //MARK:String Get From UserDefult...................
    static func stringGetFromUserDefult(keyName:String) -> String
    {
        let userDefaults: UserDefaults = UserDefaults.standard
        let watchlistData = userDefaults.object(forKey: keyName)
        
        if let watchlist : String = NSKeyedUnarchiver.unarchiveObject(with: watchlistData as! Data) as! String?
        {
            // print(watchlist)
            return watchlist
        }
        return (0 as AnyObject) as! String
        
    }
    //MARK:......................47...............................
    //MARK:Dictionary get From Read Plist ...................
    static func readPlist(plistName:String) -> [String:Any]
    {
        var swiftDictionary = [String:Any]()
        if let url = Bundle.main.url(forResource:plistName, withExtension: "plist")
        {
            do {
                let data = try Data(contentsOf:url)
                swiftDictionary = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String : Any]
                print(swiftDictionary)
                return swiftDictionary
                // do something with the dictionary
            } catch {
                print(error)
            }
        }
        return swiftDictionary
    }
    
    //MARK:......................47...............................
    //MARK:txtFileReadFromBundle ...................
    static func txtFileReadFromBundle(File name:String) -> [String]
    {
        if let path = Bundle.main.path(forResource: name, ofType: "txt") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                let myStrings = data.components(separatedBy: .newlines)
                print(myStrings)
                return myStrings
            } catch {
                print(error)
            }
        }
        return []
    }
    
    //MARK:......................47...............................
    //MARK:Text Save Into DocumentDirectory..................
    static func txtSaveIntoDocumentDirectory(FileName name:String, SaveText text:String)
    {
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = DocumentDirURL.appendingPathComponent(name).appendingPathExtension("txt")
        print("FilePath: \(fileURL.path)")
        if FileManager.default.fileExists(atPath: fileURL.path)
        {
            print("File Already Exists:Please give Another File Name:");
        }
        else
        {
            do
            {
                // Write to the file
                try text.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            } catch let error as NSError
            {
                print("Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
            }
        }
    }
    
    //MARK:......................47...............................
    //MARK:Text Read From DocumentDirectory..................
    static func txtReadFromDocumentDirectory(FileName name:String) -> String
    {
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = DocumentDirURL.appendingPathComponent(name).appendingPathExtension("txt")
        var readString = "" // Used to store the file contents
        
        if FileManager.default.fileExists(atPath: fileURL.path)
        {
            do {
                // Read the file contents
                readString = try String(contentsOf: fileURL)
                return readString
            } catch let error as NSError {
                print("Failed reading from URL: \(fileURL), Error: " + error.localizedDescription)
            }
        }
        else
        {
            print("FileName does not Match:Please Give The Correct Name//" );
        }
        
        print("File Text: \(readString)")
        return readString
    }
    //MARK:................................48......................................
    //MARK: GOOGLE_GECODE_URL  Api............................................
    static func googleGecodeApi() -> String
    {
        let myOptionalString: String? = GOOGLE_GECODE_URL
        
        guard let myString = myOptionalString, !myString.isEmpty, myString != "" , myString.characters.count > 0    else {
            print("String is nil or empty.")
            return ("url i snill" as String) // or break, continue, throw
        }
        return myString
    }
    //MARK:................................48......................................
    //MARK: GOOGLE_DIRECTIONS_URL  Api............................................
    static func googleDirectionsApi() -> String
    {
        let myOptionalString: String? = GOOGLE_DIRECTIONS_URL
        
        guard let myString = myOptionalString, !myString.isEmpty, myString != "" , myString.characters.count > 0    else {
            print("String is nil or empty.")
            return ("url i snill" as String) // or break, continue, throw
        }
        return myString
    }
    //MARK:................................48......................................
    //MARK: Show Alert............................................
    static func showAlert(Title name:String , Message message:String, Controller vc:UIViewController, Time seconds: Int)
    {
        let alertController = UIAlertController(title: name, message: message, preferredStyle: .alert)
        vc.present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(seconds)) {
            print("1m μs seconds later")
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:................................48......................................
    //MARK: Apply Circle Shadow On View............................................
    static func applyCircleShadowOnView(viewToWorkUpon:UIView, borderColor:UIColor,borderWidth:CGFloat,shadowColor:UIColor, radius:CGFloat, offset:CGSize, opacity:Float){
        
        var shadowFrame = CGRect.zero // Modify this if needed
        shadowFrame.size.width = 0.0
        shadowFrame.size.height = 0.0
        shadowFrame.origin.x = 0.0
        shadowFrame.origin.y = 0.0
        
        viewToWorkUpon.layer.cornerRadius = 5.0
        viewToWorkUpon.layer.borderColor = borderColor.cgColor
        viewToWorkUpon.layer.borderWidth = borderWidth
        viewToWorkUpon.layer.masksToBounds = true
        
        let shadow = UIView(frame: shadowFrame)//[[UIView alloc] initWithFrame:shadowFrame];
        shadow.isUserInteractionEnabled = false; // Modify this if needed
        shadow.layer.shadowColor = shadowColor.cgColor
        shadow.layer.shadowOffset = offset
        shadow.layer.shadowRadius = radius
        shadow.layer.masksToBounds = false
        shadow.clipsToBounds = false
        shadow.layer.shadowOpacity = opacity
        viewToWorkUpon.superview?.insertSubview(shadow, belowSubview: viewToWorkUpon)
        shadow.addSubview(viewToWorkUpon)
        
    }
    //MARK:........................49..............................................
    //MARK: Apply AnimationShadow On View............................................
    static func applyAnimationShadow(view: UIView) {
        let layer = view.layer
        
        //applying the shadow
        layer.shadowColor =  UIColor(white: 0.0, alpha: 0.5).cgColor
        layer.shadowOffset = CGSize(width: 20, height: -20)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 2.0
        
        //applying animation to shadows
        let animation = CABasicAnimation(keyPath: "shadowOffset")
        animation.fromValue = NSValue(cgSize:CGSize(width: -20, height: -20))
        animation.toValue =  NSValue(cgSize: CGSize(width: 20, height: -20))
        animation.duration = 2.0
        layer.add(animation, forKey: "shadowOffset")
        
    }
    //MARK:........................50..............................................
    //MARK: Apply NormalFullShadow On View............................................
    static  func applyNormalFullShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 15.0
        
    }
    //MARK:.......................51...............................................
    //MARK: Apply OverallShadow On View............................................
    static func applyOverallShadow(view: UIView, shadowOpacity:Float, shadowRadius:CGFloat, shadowColor:UIColor) {
        let layer = view.layer
        
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        
    }
    //MARK:.........................52............................................
    //MARK: Apply TrapzadolShadow On View............................................
    static func applyTrapzadolShadow(view: UIView) {
        let layer = view.layer
        
        let size = view.bounds.size
        
        //creating a trapezoidal path for shadow
        let path = UIBezierPath()
        path.move(to: CGPoint(x: size.width * 0.20, y: size.height * 0.80))
        path.addLine(to: CGPoint(x: size.width * 0.80, y: size.height * 0.80))
        path.addLine(to:  CGPoint(x: size.width * 1.20, y: size.height * 1.20))
        path.addLine(to:  CGPoint(x: size.width * -0.20, y: size.height * 1.20))
        path.close()
        
        //applying path as shadow to image
        layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        layer.shadowOffset = CGSize(width: 3, height: 3)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 2.0
        layer.shadowPath = path.cgPath
    }
    //MARK:.........................53.............................................
    //MARK: Apply EllipticalShadow On View............................................
    static func applyEllipticalShadow(view: UIView) {
        let layer = view.layer
        //create elliptical shdow forimage through UIBezierPath
        let ovalRect = CGRect(x: 0.0, y: view.frame.size.height + 10, width: view.frame.size.width, height: 15)
        let path = UIBezierPath(ovalIn: ovalRect)
        
        //applying shadow to path
        layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 3.0
        layer.shadowPath = path.cgPath
    }
    //MARK:............................54..........................................
    //MARK: ApplyCurlShadow On View............................................
    static func applyCurlShadow(view: UIView) {
        let layer = view.layer
        //create elliptical shdow forimage through UIBezierPath
        let size = view.frame.size
        let path = UIBezierPath()
        
        //starting from left point
        path.move(to: CGPoint(x: 0.0 , y: size.height))
        path.addLine(to: CGPoint(x: 0.0 , y: size.height + 20))
        
        //curved bottom part
        path.addCurve(to: CGPoint(x: size.width , y: size.height + 20), controlPoint1: CGPoint(x: 20 , y: size.height), controlPoint2: CGPoint(x: size.width - 20 , y: size.height))
        
        //closing the path by going upper top part
        path.addLine(to: CGPoint(x: size.width , y: size.height))
        
        //close the path and apply the path as shadow
        path.close()
        
        //applying shadow to imageView through the path created
        layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 3.0
        layer.shadowPath = path.cgPath
    }
    
    
    static func canOpenURL(string: String?) -> Bool {
        guard let urlString = string else {return false}
        guard let url = NSURL(string: urlString) else {return false}
        if !UIApplication.shared.canOpenURL(url as URL) {return false}
        
        //
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }
    static func validateUrl (stringURL : String) -> Bool {
        
        let urlRegEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
        //var urlTest = NSPredicate.withSubstitutionVariables(predicate)
        
        return predicate.evaluate(with: stringURL)
    }
    
    
    static func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
   
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            
            }.resume()
    }
    
    static func downloadImage(url: URL) {
        print("Download Started")
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                print("Download Finished")
                let image = UIImage(data: data!)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
               // appDelegate.image = image
            }
        
        
   //     getDataFromUrl(url: url) { data, response, error in
//            guard let data = data, error == nil else { return }
//            print(response?.suggestedFilename ?? url.lastPathComponent)
//            print("Download Finished")
//            DispatchQueue.global(qos: .background).async  {
//                let data = try? Data(contentsOf: url)
//                image = UIImage(data: data!)
//                DispatchQueue.main.async {
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.image = image
//                }
//            }
        }
    }
    
    //MARK:-  PUSH MESSAGE ALERT SHOW AND NAVIAGTE NEXT VIEW CONTROLLER
    /* static func pushShowMessageAlert()
     {
     let alertController = UIAlertController(title: "fnskf", message: "jkhjsafj", preferredStyle: .alert)
     self.present(alertController, animated: true, completion: {() -> Void in
     DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(12), execute: {() -> Void in
     
     alertController.dismiss(animated: false, completion: nil)
     
     guard let destVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as? SettingViewController else {
     print("View controller not found")
     return
     }
     self.navigationController?.pushViewController(destVC, animated: true)
     })
     })
     
     
     
     }*/
    //MARK:-  POP MESSAGE ALERT SHOW AND NAVIAGTE PREVIUOS VIEW CONTROLLER
    /*
     static func popShowMessageAlert()
     {
     let alertController = UIAlertController(title: "fnskf", message: "jkhjsafj", preferredStyle: .alert)
     self.present(alertController, animated: true, completion: {() -> Void in
     DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(seconds), execute: {() -> Void in
     
     alertController.dismiss(animated: false, completion: nil)
     
     if let navController = self.navigationController {
     navController.popViewController(animated: true)
     }
     
     })
     })
     }*/
    
    
}
