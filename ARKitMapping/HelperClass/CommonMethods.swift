//
//  CommonMethods.swift
//  ARKitMapping
//
//  Created by Anvesh on 20/06/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

class CommonMethods: NSObject {
    static let sharedInstance = CommonMethods()

    private override init() {}
    public func distanceBetweenTwoVectorsUsingSCNVector(vectorPointsFrom:SCNVector3, vectorPointsTo:SCNVector3) -> Float{
        let distance = SCNVector3(
            vectorPointsFrom.x - vectorPointsTo.x,
            vectorPointsFrom.y - vectorPointsTo.y,
            vectorPointsFrom.z - vectorPointsTo.z
        )
        let length: Float = sqrtf(distance.x * distance.x + distance.y * distance.y + distance.z * distance.z)
        return length
    }
    
    
    public func distanceBetweenTwoVectors(vectorPointsFrom:Vector, vectorPointsTo:Vector) -> Float{
        let distance = SCNVector3(
            vectorPointsFrom.x - vectorPointsTo.x,
            vectorPointsFrom.y - vectorPointsTo.y,
            vectorPointsFrom.z - vectorPointsTo.z
        )
        let length: Float = sqrtf(distance.x * distance.x + distance.y * distance.y + distance.z * distance.z)
        return length
    }
}
