//
//  WebServiceSessionHelper.swift
//  MySwiftExample
//
//  Created by anupama on 17/01/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit
import Foundation
import CoreFoundation

//var URI_HTTP = String(format: "http://192.168.1.45:3000/")
var URI_HTTP = String(format: "http://138.197.13.173")
//var URI_HTTP = String(format: "www.gharwale.com")
//MARK:  API_METHOD..................
var POST_METHOD                     = String(format: "POST")
var GET_METHOD                      = String(format: "GET")
var PUT_METHOD                      = String(format: "PUT")
var DELETE_METHOD                   = String(format: "DELETE")
var PATCH_METHOD                   = String(format: "PATCH")

//MARK:  HTTP_HEADER DEFINED KEY..................
var HEADER_CONTENT_LENGTH           = String(format: "Content-Length")
var HEADER_CONTENT_TYPE             = String(format: "Content-Type")
var HEADER_CONTENT_TYPE_VALUE       = String(format: "application/x-www-form-urlencoded")
var HEADER_ACEEPT                   = String(format: "Accept")
var HEADER_ACEEPT_VALUE             = String(format: "application/json")

var RUBY_HEADER_CONTENT_TYPE             = String(format: "Content-Type")
var RUBY_HEADER_CONTENT_TYPE_VALUE             = String(format: "application/json")


var MULTIPART_BOUNDARY   = String(format: "---------------------------14737809831466499882746641449")
var MULTIPART_CONTENT_TYPE   = String(format:"multipart/form-data; boundary=%@")

var HEADER_AUTHTOKEN                = String(format: "X-User-Token")
var HEADER_EMAIL                    = String(format: "X-User-Email")


@objc protocol WebServiceSessionHelperDelegate
{
    func gettingFail(error: String)
    
    @objc optional func loginSuccess(response: Data)
    @objc optional func changePassword(response: Data)
    @objc optional func registration(response: Data)
    @objc optional func forgotPassword(response: Data)
    @objc optional func registrationWithPic(response: Data)
    @objc optional func showUserProfile(response: Data)
    @objc optional func editProfile(response: Data)
    @objc optional func showFavourite(response: Data)
    @objc optional func addFavourite(response: Data)
    @objc optional func deleteFavourite(response: Data)
    @objc optional func uploadPicture(response: Data)
    @objc optional func uploadStory(response: Data)
    @objc optional func uploadMessage(response: Data)
    @objc optional func downloadStory(response: Data)
    
    @objc optional func post(response: Data)
    @objc optional func post1(response: Data)
    @objc optional func post2(response: Data)
    @objc optional func post3(response: Data)
    
    @objc optional func insertMLSID(response: Data)
    @objc optional func deleteMLSID(response: Data)
    @objc optional func profileEditing(response: Data)
    @objc optional func notesUpdating(response: Data)
    @objc optional func notesDeleting(response: Data)
    @objc optional func historyLog(response: Data)
    @objc optional func realtorShow(response: Data)
    @objc optional func notesgetting(response: Data)
    @objc optional func updateStatus(response: Data)
    @objc optional func callDelete(response: Data)
    @objc optional func postphp(response: Data)
    
    // MARK:  POST  Api Method Response...................
    @objc optional func postApiMethodResponse(responseData: Data)
    @objc optional func postApiMethodResponse1(responseData: Data)
    @objc optional func postApiMethodResponse2(responseData: Data)

    // MARK:  GET  Api Method Response...................
    @objc optional func getApiMethodResponse(responseData: Data)
    @objc optional func getApiMethodResponse1(responseData: Data)
    
    // MARK:  PUT  Api Method Response...................
    @objc optional func putApiMethodResponse(responseData: Data)
    @objc optional func putApiMethodResponse1(responseData: Data)
    
    // MARK:  DELETE  Content Type  Api Method Response...................
    @objc optional func deleteApiMethodResponse(responseData: Data)
    @objc optional func deleteApiMethodResponse1(responseData: Data)
    @objc optional func deleteApiMethodResponse2(responseData: Data)
    
    // MARK:  DELETE  No Content Type  Api Method Response...................
    @objc optional func delNoContentTypeMethodResponse(responseData: Data)
    @objc optional func delNoContentTypeMethodResponse1(responseData: Data)
    @objc optional func delNoContentTypeMethodResponse2(responseData: Data)
    
     // MARK:  PATCH  Api Method Response...................
    @objc optional func patchApiMethodResponse(responseData: Data)
    @objc optional func patchApiMethodResponse1(responseData: Data)
    @objc optional func patchApiMethodResponse2(responseData: Data)
    @objc optional func patchApiMethodResponse3(responseData: Data)
    @objc optional func patchApiMethodResponse4(responseData: Data)

}

class WebServiceSessionHelper : NSObject, URLSessionDataDelegate, URLSessionDelegate, URLSessionTaskDelegate
{
    var serviceName = String()
    var responseString :String! = nil
    weak var  delegate: WebServiceSessionHelperDelegate?
    
    //MARK:PHP POST METHOD IN STRING....
    // MARK:...........................................................
    // MARK:...Request With Post.Url and Paramter in String..................
    func jsonPaseringWithPHP(datastring: String, urlString: String)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let url = URL(string: urlString)!
        let postData = datastring.data(using: String.Encoding.ascii) as Data!
        let postLength =  String(format: "%lu", (postData?.count)!)
        var request = URLRequest(url: url)
        request.httpMethod = POST_METHOD
        request.setValue(postLength, forHTTPHeaderField: HEADER_CONTENT_LENGTH)
        request.setValue(HEADER_ACEEPT_VALUE, forHTTPHeaderField: HEADER_ACEEPT)
        request.setValue(HEADER_CONTENT_TYPE_VALUE, forHTTPHeaderField: HEADER_CONTENT_TYPE)
        request.httpBody = postData
        let dataTask = defaultSession.dataTask(with: request as URLRequest)
        dataTask.resume()
        
    }
    // MARK:...........................................................
    // MARK:...Request With Url .................
    func requestWithURL(urlString: String)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let  defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let url = URL(string: urlString)!
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = GET_METHOD
        request.setValue(HEADER_ACEEPT_VALUE, forHTTPHeaderField: HEADER_ACEEPT)
        request.setValue(HEADER_CONTENT_TYPE_VALUE, forHTTPHeaderField: HEADER_CONTENT_TYPE)
        let dataTask = defaultSession.dataTask(with: request as URLRequest)
        dataTask.resume()
        
    }
    // MARK:...........................................................
    // MARK:...Request With Url and Data..................
    func requestWithURLPic(urlString: String, picData: NSData)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let   defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let url = URL(string: urlString)!
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = POST_METHOD
        request.setValue(String(format: "%lu", (picData.length)), forHTTPHeaderField: HEADER_CONTENT_LENGTH)
        request.setValue(HEADER_ACEEPT_VALUE, forHTTPHeaderField: HEADER_ACEEPT)
        request.setValue(HEADER_CONTENT_TYPE_VALUE, forHTTPHeaderField: HEADER_CONTENT_TYPE)
        let dataTask = defaultSession.dataTask(with: request as URLRequest)
        dataTask.resume()
        
    }
    // MARK:...........................................................
    // MARK:...Request With Post.Url and Dictionary..................
    func requestWithPost(urlString: String, Dict: NSDictionary)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: Dict, options: .prettyPrinted) as Data!
            // here "jsonData" is the dictionary encoded in JSON data
            
            let url = URL(string: urlString)!
            let postRequest = NSMutableURLRequest(url: url)
            postRequest.httpMethod = POST_METHOD
            postRequest.setValue(String(format: "%lu", (jsonData?.count)!), forHTTPHeaderField: HEADER_CONTENT_LENGTH)
            postRequest.setValue(HEADER_ACEEPT_VALUE, forHTTPHeaderField: HEADER_ACEEPT)
            postRequest.setValue(HEADER_CONTENT_TYPE_VALUE, forHTTPHeaderField: HEADER_CONTENT_TYPE)
            postRequest.httpBody = jsonData
            let dataTask = defaultSession.dataTask(with: postRequest as URLRequest)
            dataTask.resume()
        }
        catch
        {
            print(error.localizedDescription)
        }
    }
    // MARK:...........................................................
    // MARK:...Post Image and Parameter On PHPserver...................
    func postImageandParameterOnPHPserver(urlString: String, uploadImage: String,  parameters: Dictionary<String,AnyObject>?)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let myImage = UIImage.init(named: uploadImage)
        let imageData:NSData = UIImagePNGRepresentation(myImage!)! as NSData
        
        let url = URL(string: urlString)!
        let postRequest = NSMutableURLRequest(url: url)
        postRequest.httpMethod = POST_METHOD
        //let boundary:String = "---------------------------14737809831466499882746641449"
        //let contentType:String = "multipart/form-data; boundary=%@"
        postRequest.setValue(MULTIPART_CONTENT_TYPE, forHTTPHeaderField: HEADER_CONTENT_TYPE)
        let mybodyData:NSMutableData = NSMutableData();
        mybodyData.append(String(format: "\r\n--%@\r\n", MULTIPART_BOUNDARY).data(using: String.Encoding.utf8)!)
        mybodyData.append(String(format: "Content-Disposition: form-data; name=\"userfile\"; filename=\"ipodfile.jpg\"\r\n", MULTIPART_BOUNDARY).data(using: String.Encoding.utf8)!)
        mybodyData.append(String(format: "Content-Type: application/octet-stream\r\n\r\n", MULTIPART_BOUNDARY).data(using: String.Encoding.utf8)!)
        mybodyData.append(imageData as Data)
        mybodyData.append(String(format: "\r\n--%@--\r\n", MULTIPART_BOUNDARY).data(using: String.Encoding.utf8)!)
        
        if parameters != nil
        {
            for (key, value) in parameters!
            {
                mybodyData.append(String(format: "--%@\r\n", MULTIPART_BOUNDARY).data(using: String.Encoding.utf8)!)
                mybodyData.append(String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key).data(using: String.Encoding.utf8)!)
                mybodyData.append(String(format: "%@", value as! CVarArg).data(using: String.Encoding.utf8)!)
                mybodyData.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        mybodyData.append(String(format: "--%@--\r\n", MULTIPART_BOUNDARY).data(using: String.Encoding.utf8)!)
        postRequest.httpBody = mybodyData as Data
        
        let dataTask = defaultSession.dataTask(with: postRequest as URLRequest)
        dataTask.resume()
        
    }
    
    func login(param : String)
    {
        serviceName="Login"
        let postString = String(format: "%@%@", URI_HTTP,param)
        requestWithURL(urlString: postString)
    }
    func changePassword(urlString : String)
    {
        serviceName="ChangePassword"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func registration(urlString : String)
    {
        serviceName="Registration"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func registrationWithPic(urlString : String)
    {
        serviceName="RegistrationPic"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func forgotPassword(urlString : String)
    {
        serviceName="ForgotPassword"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func showUserProfile(urlString : String)
    {
        serviceName="ShowUserPfrofile"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func editProfile(urlString : String)
    {
        serviceName="EditPfrofile"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func showFavourite(urlString : String)
    {
        serviceName="ShowFavourite"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func addFavourite(urlString : String)
    {
        serviceName="AddFavourite"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func deleteFavourite(urlString : String)
    {
        serviceName="DeleteFavourite"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func uploadPicture(urlString : String)
    {
        serviceName="UploadPicture"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func uploadStory(urlString : String)
    {
        serviceName="UploadStory"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func uploadMessage(urlString : String)
    {
        serviceName="UploadMessage"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func downloadStory(urlString : String)
    {
        serviceName="DownloadStory"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    
    // MARK:  Post method...................
    func postphp(urlString : String, uploadImage : String, params : NSDictionary)
    {
        serviceName="Postphp"
        postImageandParameterOnPHPserver(urlString: urlString, uploadImage: uploadImage, parameters: params as? Dictionary<String, AnyObject>)
    }
    
    func post1(urlString : String, params : String)
    {
        serviceName="Post1"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func post2(urlString : String, data : NSData)
    {
        serviceName="Post2"
        requestWithURLPic(urlString: urlString, picData: data)
    }
    func post3(urlString : String, params : String)
    {
        serviceName="Post3"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func callDelete(urlString : String, params : String)
    {
        serviceName="Post4"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func insertMLSID(urlString : String, params : String)
    {
        serviceName="Post5"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func deleteMLSID(urlString : String, params : String)
    {
        serviceName="Post6"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func profileEditing(urlString : String, params : String)
    {
        serviceName="Post7"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func notesUpdating(urlString : String, params : String)
    {
        serviceName="Post8"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func notesDeleting(urlString : String, params : String)
    {
        serviceName="Post9"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func historyLog(urlString : String, params : String)
    {
        serviceName="Post10"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func realtorShow(urlString : String, params : String)
    {
        serviceName="Post11"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func notesgetting(urlString : String, params : String)
    {
        serviceName="Post12"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func updateStatus(urlString : String, params : String)
    {
        serviceName="Post13"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    
    // MARK:- URLSessionDownloadDelegate
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data)
    {
        print("Did receive data!",data)
        
        URLSessionDidFinishLoading(reciveData: data)
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {
        
        if (error != nil) {
            print("didCompleteWithError \(String(describing: error?.localizedDescription))")
            delegate?.gettingFail(error: (error?.localizedDescription)! )
        }
        else {
            print("The task finished successfully")
        }
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void)
    {
        print("The task finished successfully",response)
        let responseStatusCode:Int? = (dataTask.response as! HTTPURLResponse).statusCode
        print(responseStatusCode as Any)
        guard let statusCode = responseStatusCode, statusCode == 200
            else {
                print("errorr")
                 let responseStatusMessage = (dataTask.response as! HTTPURLResponse).allHeaderFields["Status"]
                print(responseStatusMessage as Any)
                
                 self.delegate?.gettingFail(error: "statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
            return
        }
        completionHandler(.allow)
        
    }
    
    // MARK:...........................................................
    // MARK:...Url Session Finish method...................
    func URLSessionDidFinishLoading(reciveData :Data)
    {
        responseString = String(data: reciveData, encoding: String.Encoding.utf8)!
        print(responseString)
        print(serviceName)
        print(reciveData)
        if serviceName.isEqual("Login")
        {
            delegate?.loginSuccess!(response: reciveData)
        }
        else if serviceName.isEqual("ChangePassword")
        {
            delegate?.changePassword!(response: reciveData)
        }
        else if serviceName.isEqual("Registration")
        {
            delegate?.registration!(response: reciveData )
        }
        else if serviceName.isEqual("ForgotPassword")
        {
            self.delegate?.forgotPassword!(response: reciveData )
        }
        else if serviceName.isEqual("ShowUserPfrofile")
        {
            delegate?.showUserProfile!(response: reciveData )
        }
        else if serviceName.isEqual("EditPfrofile")
        {
            delegate?.editProfile!(response: reciveData )
        }
        else if serviceName.isEqual("ShowFavourite")
        {
            delegate?.showFavourite!(response: reciveData )
        }
        else if serviceName.isEqual("AddFavourite")
        {
            delegate?.addFavourite!(response: reciveData )
        }
        else if serviceName.isEqual("DeleteFavourite")
        {
            delegate?.deleteFavourite!(response: reciveData )
        }
        else if serviceName.isEqual("UploadPicture")
        {
            delegate?.uploadPicture!(response: reciveData )
        }
        else if serviceName.isEqual("UploadStory")
        {
            delegate?.uploadStory!(response: reciveData )
        }
        else if serviceName.isEqual("UploadMessage")
        {
            delegate?.uploadMessage!(response: reciveData )
        }
        else if serviceName.isEqual("DownloadStory")
        {
            delegate?.downloadStory!(response: reciveData )
        }
        else if serviceName.isEqual("Post")
        {
            delegate?.post!(response: reciveData )
        }
        else if serviceName.isEqual("Post1")
        {
            delegate?.post1!(response: reciveData )
        }
        else if serviceName.isEqual("Post2")
        {
            delegate?.post2!(response: reciveData )
        }
        else if serviceName.isEqual("Post3")
        {
            delegate?.post3!(response: reciveData )
        }
        else if serviceName.isEqual("Post4")
        {
            delegate?.callDelete!(response: reciveData )
        }
        else if serviceName.isEqual("Post5")
        {
            delegate?.insertMLSID!(response: reciveData )
        }
        else if serviceName.isEqual("Post6")
        {
            delegate?.deleteMLSID!(response: reciveData )
        }
        else if responseString.isEqual("Post7")
        {
            delegate?.profileEditing!(response: reciveData )
        }
        else if serviceName.isEqual("Post8")
        {
            delegate?.notesUpdating!(response: reciveData )
        }
        else if serviceName.isEqual("Post9")
        {
            delegate?.notesDeleting!(response: reciveData )
        }
        else if serviceName.isEqual("Post10")
        {
            delegate?.historyLog!(response: reciveData )
        }
        else if serviceName.isEqual("Post11")
        {
            delegate?.realtorShow!(response: reciveData )
        }
        else if serviceName.isEqual("Post12")
        {
            delegate?.notesgetting!(response: reciveData )
        }
        else if serviceName.isEqual("Post13")
        {
            delegate?.updateStatus!(response: reciveData )
        }
        else if serviceName.isEqual("Postphp")
        {
            delegate?.postphp!(response: reciveData )
        }
    }
    func urlSession(connection: URLSession, canAuthenticateAgainstProtectionSpace protectionSpace: URLProtectionSpace?) -> Bool
    {
        if protectionSpace?.authenticationMethod == NSURLAuthenticationMethodServerTrust
        {
            return true
        }
        else
        {
            if protectionSpace?.authenticationMethod == NSURLAuthenticationMethodHTTPBasic
            {
                return true
            }
            
        }
        return false
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
    {
        var disposition: URLSession.AuthChallengeDisposition = URLSession.AuthChallengeDisposition.performDefaultHandling
        
        var credential:URLCredential?
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust
        {
            credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            if (credential != nil)
            {
                disposition = URLSession.AuthChallengeDisposition.useCredential
            }
            else
            {
                disposition = URLSession.AuthChallengeDisposition.performDefaultHandling
            }
        }
        else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic
        {
            credential = URLCredential(user: "test",
                                       password: "test",
                                       persistence:.none)
        }
        else
        {
            disposition = URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge
            
        }
        completionHandler(disposition, credential)
        challenge.sender?.continueWithoutCredential(for: challenge)
    }
    
    // MARK:  Post  Api method...................
    func postApiMethod(urlString : String, params Dict: NSMutableDictionary, Auth_token : String, User_Email : String)
    {
        serviceName="PostApi"
        postRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func postApiMethod1(urlString : String,params Dict: NSMutableDictionary, Auth_token : String, User_Email : String)
    {
        serviceName="PostApi1"
        postRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func postApiMethod2(urlString : String,params Dict: NSMutableDictionary, Auth_token : String, User_Email : String)
    {
        serviceName="PostApi2"
        postRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func postRubyDataMethod(urlString: String,params Dict: NSMutableDictionary,Auth_Token tokenValue: String,User_Email emailID: String) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        
        let url = URL(string: urlString)!
        var postRequest = URLRequest(url: url)
        postRequest.httpMethod = "POST"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: Dict, options: .prettyPrinted)
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            let postData = jsonString?.data(using: String.Encoding.ascii, allowLossyConversion: true)
            let postLength =  String(format: "%lu", (postData?.count)!)
            postRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            postRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            postRequest.addValue(tokenValue, forHTTPHeaderField: "Auth-Token")
            postRequest.addValue(emailID, forHTTPHeaderField: "X-User-Email")
            postRequest.httpBody = jsonString?.data(using: String.Encoding.utf8)
            
            let task = defaultSession.dataTask(with: postRequest as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else
                {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    //self.delegate?.gettingFail(error: "error=\(String(describing: error?.localizedDescription))")
                    self.delegate?.gettingFail(error:String(format: "%@", (error?.localizedDescription)!))
                    return
                }
                guard let data = data else { return }
                guard let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 else
                {
                    // check for http errors
                    print("statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    print(response!)
                    self.delegate?.gettingFail(error: "statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    return
                }
                DispatchQueue.main.async
                    {
                        if self.serviceName.isEqual("PostApi")
                        {
                            self.delegate?.postApiMethodResponse!(responseData: data)
                        }
                        else if self.serviceName.isEqual("PostApi1")
                        {
                            self.delegate?.postApiMethodResponse1!(responseData: data)
                        }
                        else if self.serviceName.isEqual("PostApi2")
                        {
                            self.delegate?.postApiMethodResponse2!(responseData: data)
                        }
                        
                }
                
            })
            task.resume()
        } catch let error {
            print(error.localizedDescription)
        }
        
        
    }
    
    // MARK:  Get Api method...................
    func getApiMethod(urlString : String, Auth_token : String, User_Email : String)
    {
        serviceName="GetApi"
        getRubyDataMethod(urlString: urlString, auth_token: Auth_token, email: User_Email)
    }
    func getApiMethod1(urlString : String, Auth_token : String, User_Email : String)
    {
        serviceName="GetApi1"
        getRubyDataMethod(urlString: urlString, auth_token: Auth_token, email: User_Email)
    }
    func getRubyDataMethod(urlString: String,auth_token: String, email: String) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(auth_token, forHTTPHeaderField: "Auth-Token")
        request.addValue(email, forHTTPHeaderField: "X-User-Email")
        
        let task = defaultSession.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else
            {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                //self.delegate?.gettingFail(error: "error=\(String(describing: error?.localizedDescription))")
                self.delegate?.gettingFail(error:String(format: "%@", (error?.localizedDescription)!))
                return
            }
            guard let data = data else { return }
            guard let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 else
            {
                // check for http errors
                print("statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                print(response!)
                self.delegate?.gettingFail(error: "statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                return
            }
            DispatchQueue.main.async
                {
                    if self.serviceName.isEqual("GetApi")
                    {
                        self.delegate?.getApiMethodResponse!(responseData: data)
                    }
                    else if self.serviceName.isEqual("GetApi1")
                    {
                        self.delegate?.getApiMethodResponse1!(responseData: data)
                    }
                    
            }
        })
        task.resume()
    }
    
    // MARK:  PUT  Api method...................
    func putApiMethod(urlString : String, params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="PutApi"
        putRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func putApiMethod1(urlString : String,params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="PutApi1"
        putRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func putRubyDataMethod(urlString: String,params Dict: [String:Any],Auth_Token tokenValue: String,User_Email emailID: String)
    {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        
        let url = URL(string: urlString)!
        var postRequest = URLRequest(url: url)
        postRequest.httpMethod = "PUT"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: Dict, options: .prettyPrinted)
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            let postData = jsonString?.data(using: String.Encoding.ascii, allowLossyConversion: true)
            let postLength =  String(format: "%lu", (postData?.count)!)
            postRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            postRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            postRequest.addValue(tokenValue, forHTTPHeaderField: "Auth-Token")
            postRequest.addValue(emailID, forHTTPHeaderField: "X-User-Email")
            postRequest.httpBody = jsonString?.data(using: String.Encoding.utf8)
            
            let task = defaultSession.dataTask(with: postRequest as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else
                {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    //self.delegate?.gettingFail(error: "error=\(String(describing: error?.localizedDescription))")
                    //self.delegate?.gettingFail(error:String(format: "%@", (error?.localizedDescription)!))
                    self.delegate?.gettingFail(error:String(format: "%@", (error?.localizedDescription)!))
                    return
                }
                guard let data = data else { return }
                guard let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 else
                {
                    // check for http errors
                    print("statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    print(response!)
                    self.delegate?.gettingFail(error: "statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    return
                }
                DispatchQueue.main.async
                    {
                        if self.serviceName.isEqual("PutApi")
                        {
                            self.delegate?.putApiMethodResponse!(responseData: data)
                        }
                        else if self.serviceName.isEqual("PutApi1")
                        {
                            self.delegate?.putApiMethodResponse1!(responseData: data)
                        }
                        
                }
                
            })
            task.resume()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    // MARK:  Delete  Api method...................
    func deleteApiMethod(urlString : String, params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="DeleteApi"
        deleteRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func deleteApiMethod1(urlString : String,params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="DeleteApi1"
        deleteRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func deleteApiMethod2(urlString : String,params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="DeleteApi2"
        deleteRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func deleteRubyDataMethod(urlString: String,params Dict: [String:Any],Auth_Token tokenValue: String,User_Email emailID: String) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        
        let url = URL(string: urlString)!
        var postRequest = URLRequest(url: url)
        postRequest.httpMethod = "DELETE"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: Dict, options: .prettyPrinted)
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            let postData = jsonString?.data(using: String.Encoding.ascii, allowLossyConversion: true)
            let postLength =  String(format: "%lu", (postData?.count)!)
            postRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            postRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            postRequest.addValue(tokenValue, forHTTPHeaderField: "Auth-Token")
            postRequest.addValue(emailID, forHTTPHeaderField: "X-User-Email")
            postRequest.httpBody = jsonString?.data(using: String.Encoding.utf8)
            
            let task = defaultSession.dataTask(with: postRequest as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else
                {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    //self.delegate?.gettingFail(error: "error=\(String(describing: error?.localizedDescription))")
                    self.delegate?.gettingFail(error:String(format: "%@", (error?.localizedDescription)!))
                    return
                }
                guard let data = data else { return }
                guard let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 else
                {
                    // check for http errors
                    print("statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    print(response!)
                    self.delegate?.gettingFail(error: "statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    return
                }
                DispatchQueue.main.async
                    {
                        if self.serviceName.isEqual("DeleteApi")
                        {
                            self.delegate?.deleteApiMethodResponse!(responseData: data)
                        }
                        else if self.serviceName.isEqual("DeleteApi1")
                        {
                            self.delegate?.deleteApiMethodResponse1!(responseData: data)
                        }
                        else if self.serviceName.isEqual("DeleteApi2")
                        {
                            self.delegate?.deleteApiMethodResponse2!(responseData: data)
                        }
                        
                }
                
            })
            task.resume()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    // MARK:  Delete  No Content Type  Api method...................
    func delNoContentTypeMethod(urlString : String, Auth_token : String, User_Email : String)
    {
        serviceName="DeleteNoContentApi"
        deleteRubyWithoutContentMethod(urlString: urlString, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func delNoContentTypeMethod1(urlString : String, Auth_token : String, User_Email : String)
    {
        serviceName="DeleteNoContentApi1"
        deleteRubyWithoutContentMethod(urlString: urlString, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func delNoContentTypeMethod2(urlString : String, Auth_token : String, User_Email : String)
    {
        serviceName="DeleteNoContentApi2"
        deleteRubyWithoutContentMethod(urlString: urlString, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func deleteRubyWithoutContentMethod(urlString: String, Auth_Token tokenValue: String,User_Email emailID: String) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        
        let url = URL(string: urlString)!
        var postRequest = URLRequest(url: url)
        postRequest.httpMethod = "DELETE"
        postRequest.addValue(tokenValue, forHTTPHeaderField: "Auth-Token")
        postRequest.addValue(emailID, forHTTPHeaderField: "X-User-Email")
        
            let task = defaultSession.dataTask(with: postRequest as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else
                {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    //self.delegate?.gettingFail(error: "error=\(String(describing: error?.localizedDescription))")
                    self.delegate?.gettingFail(error:String(format: "%@", (error?.localizedDescription)!))
                    return
                }
                guard let data = data else { return }
                guard let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 else
                {
                    // check for http errors
                    print("statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    print(response!)
                    self.delegate?.gettingFail(error: "statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    return
                }
                DispatchQueue.main.async
                    {
                        if self.serviceName.isEqual("DeleteNoContentApi")
                        {
                            self.delegate?.delNoContentTypeMethodResponse!(responseData: data)
                        }
                        else if self.serviceName.isEqual("DeleteNoContentApi1")
                        {
                            self.delegate?.delNoContentTypeMethodResponse1!(responseData: data)
                        }
                        else if self.serviceName.isEqual("DeleteNoContentApi2")
                        {
                            self.delegate?.delNoContentTypeMethodResponse2!(responseData: data)
                        }
                        
                }
                
            })
            task.resume()
        
    }

    
    // MARK:  Patch  Api method...................
    func patchApiMethod(urlString : String, params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="PatchApi"
        patchRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func patchApiMethod1(urlString : String,params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="PatchApi1"
        patchRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func patchApiMethod2(urlString : String,params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="PatchApi2"
        patchRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func patchApiMethod3(urlString : String,params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="PatchApi3"
        patchRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func patchApiMethod4(urlString : String,params Dict: [String:Any], Auth_token : String, User_Email : String)
    {
        serviceName="PatchApi4"
        patchRubyDataMethod(urlString: urlString, params: Dict, Auth_Token: Auth_token, User_Email: User_Email)
    }
    func patchRubyDataMethod(urlString: String,params Dict: [String:Any],Auth_Token tokenValue: String,User_Email emailID: String) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        
        let url = URL(string: urlString)!
        var postRequest = URLRequest(url: url)
        postRequest.httpMethod = "PATCH"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: Dict, options: .prettyPrinted)
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            let postData = jsonString?.data(using: String.Encoding.ascii, allowLossyConversion: true)
            let postLength =  String(format: "%lu", (postData?.count)!)
            postRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            postRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            postRequest.addValue(tokenValue, forHTTPHeaderField: "X-User-Token")
            postRequest.addValue(emailID, forHTTPHeaderField: "X-User-Email")
            postRequest.httpBody = jsonString?.data(using: String.Encoding.utf8)
            
            let task = defaultSession.dataTask(with: postRequest as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else
                {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                   // self.delegate?.gettingFail(error: "error=\(String(describing: error?.localizedDescription))")
                    self.delegate?.gettingFail(error:String(format: "%@", (error?.localizedDescription)!))
                    return
                }
                guard let data = data else { return }
                guard let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 else
                {
                    // check for http errors
                    print("statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    print(response!)
                    self.delegate?.gettingFail(error: "statusCode should be 200, but is \(String(describing:  (response as? HTTPURLResponse)?.statusCode))")
                    return
                }
                DispatchQueue.main.async
                    {
                        if self.serviceName.isEqual("PatchApi")
                        {
                            self.delegate?.patchApiMethodResponse!(responseData: data)
                        }
                        else if self.serviceName.isEqual("PatchApi1")
                        {
                            self.delegate?.patchApiMethodResponse1!(responseData: data)
                        }
                        else if self.serviceName.isEqual("PatchApi2")
                        {
                            self.delegate?.patchApiMethodResponse2!(responseData: data)
                        }
                        else if self.serviceName.isEqual("PatchApi3")
                        {
                            self.delegate?.patchApiMethodResponse3!(responseData: data)
                        }
                        else if self.serviceName.isEqual("PatchApi4")
                        {
                            self.delegate?.patchApiMethodResponse4!(responseData: data)
                        }
                        
                }
                
            })
            task.resume()
        } catch let error {
            print(error.localizedDescription)
        }
    }


}



