//
//  SelectClassesTableViewController.swift
//  AirnectFloorMapping
//
//  Created by Anvesh on 20/09/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import UIKit

class SelectClassesTableViewController: UITableViewController {
    var appDelegate:AppDelegate!
    var loaderView: Loader = Loader()
    var arrFirstSummary:NSMutableArray = NSMutableArray()

   
    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isSelectionOfBooks = true
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Add class"
        self.apiForFirstSummary()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    func apiForFirstSummary(){
         let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            loaderView.showLoaderOnScreen(vc: self, view:self.view)
            var dictHeader: [String:String] = [String:String]()
            dictHeader["auth_token"] =  UserDefaults.standard.value(forKey: "auth_token") as? String
            dictHeader["Device-Type"] = "iOS"
            APICallExecutor.getRequestForURLString(true, "/admin/ddc_categories", dictHeader, nil) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async{
                    self.loaderView.hideLoader()
                    if Success{
                        let dict:NSDictionary = result as! NSDictionary
                        self.arrFirstSummary = dict.value(forKey: "first_summary") as! NSMutableArray
                        print(self.arrFirstSummary)
                    }
                }
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
