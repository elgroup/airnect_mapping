//
//  SelectSectionTableViewController.swift
//  AirnectFloorMapping
//
//  Created by Anvesh on 13/08/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import UIKit

class SelectSectionTableViewController: UITableViewController {
    var dictUser:NSDictionary!
    var arrayLibraryList:NSArray!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.title = "Select Library"
        arrayLibraryList = NSArray()
        arrayLibraryList = dictUser["library_list"] as? NSArray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"back"), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        
    }
    @objc func backTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return arrayLibraryList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdetifier", for: indexPath)
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellIdetifier")
        
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cellIdetifier")
        }
        let dictLibrary:NSDictionary = arrayLibraryList.object(at: indexPath.row) as! NSDictionary
        let strLibraryName:String = dictLibrary["name"] as! String
        cell?.textLabel?.text = strLibraryName
        return cell!
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = UIStoryboard.init(name: "Main" , bundle: nil)
        let controller:AddressTableViewController = story.instantiateViewController(withIdentifier: "AddressTableViewController") as! AddressTableViewController
        controller.dictUser = dictUser
        controller.intSelectedIndex = indexPath.row
        self.navigationController?.pushViewController(controller, animated: true)
    }

    /*
    / Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
