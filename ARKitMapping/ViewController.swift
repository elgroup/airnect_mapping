/*
 See LICENSE folder for this sample’s licensing information.
 
 Abstract:
 Main view controller for the AR experience.
 */

import UIKit
import SceneKit
import ARKit
import CoreData

class ViewController: UIViewController, ARSessionDelegate,WebServiceSessionHelperDelegate {
    
    
    // MARK: - IBOutlets
    @IBOutlet weak var sessionInfoView: UIView!
    @IBOutlet weak var sessionInfoLabel: UILabel!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet var buttonSend:UIButton!
    @IBOutlet weak var viewBox: UIView!
    @IBOutlet weak var instructionLabel: UILabel!
    
    var currentPositionOfCamera:SCNVector3!
    var sphereNode = SCNNode()
    var vectorId: Int = 0
    var vertexId: Int = 0
    var lastNode: Vertex!
    var appDelegate:AppDelegate!
    var selctedNodes:NSInteger = 0
    var selectedNodeArray:NSMutableArray!
    var webService: WebServiceSessionHelper = WebServiceSessionHelper()
    var loaderView: Loader = Loader()
    var longGesture = UILongPressGestureRecognizer()
    var tapRecognizer  = UITapGestureRecognizer()
    var isScaned:Bool = false
    var navigate:UINavigationController = UINavigationController()
    var floorNumber: Int!
    var isNode = false
    var isAttach = false
    var viewAlert:UIView!
    var isScanned = false
    let updateQueue = DispatchQueue(label: Bundle.main.bundleIdentifier! +
        ".serialSceneKitQueue")
    
    
    //MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        if !appDelegate.isSelectionOfBooks{
            self.navigationController?.isNavigationBarHidden = true
            sceneView.delegate = self
            configureLighting()
        }
    }
    
    //MARK: Set up for the long press
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // buttonSave.isHidden = true
        longGesture = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.longPress(_:)))
        longGesture.minimumPressDuration = 1
        if !appDelegate.isSelectionOfBooks{
            buttonSave.isHidden = true
            self.navigationController?.isNavigationBarHidden = true
            resetTrackingConfiguration()
        }
        else{
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    
    //MARK: Set up for the ARConfiguration
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !appDelegate.isSelectionOfBooks{
            selectedNodeArray = NSMutableArray()
            
            //            viewAlert = UIView()
            //            viewAlert.frame = sceneView.bounds
            //            sceneView.addSubview(viewAlert)
            //            let label:UILabel = UILabel.init(frame: viewAlert.bounds)
            //            label.text = "Please scan the image then start mapping the floor."
            //            label.textAlignment = .center
            //            viewAlert.addSubview(label)
            //            label.numberOfLines = 0
            //            label.textColor = UIColor.white
            
            //        guard ARWorldTrackingConfiguration.isSupported else {
            //            fatalError("""
            //                ARKit is not available on this device. For apps that require ARKit
            //                for core functionality, use the `arkit` key in the key in the
            //                `UIRequiredDeviceCapabilities` section of the Info.plist to prevent
            //                the app from installing. (If the app can't be installed, this error
            //                can't be triggered in a production scenario.)
            //                In apps where AR is an additive feature, use `isSupported` to
            //                determine whether to show UI for launching AR experiences.
            //            """) // For details, see https://developer.apple.com/documentation/arkit
            //        }
            //
            
            //
            //        // Start the view's AR session with a configuration that uses the rear camera,
            //        // device position and orientation tracking, and plane detection.
            //        let configuration = ARWorldTrackingConfiguration()
            //        configuration.planeDetection = .vertical //[.horizontal, .vertical]
            //        sceneView.session.run(configuration)
            //
            //        // Set a delegate to track the number of plane anchors for providing UI feedback.
            //        sceneView.session.delegate = self
            //
            //        // Prevent the screen from being dimmed after a while as users will likely
            //        // have long periods of interaction without touching the screen or buttons.
            //        UIApplication.shared.isIdleTimerDisabled = true
            //
            //        // Show debug UI to view performance metrics (e.g. frames per second).
            //        sceneView.showsStatistics = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if !appDelegate.isSelectionOfBooks{
            sceneView.session.pause()
        }
    }
    
    //MARK: Configuring Light for ARKIT
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    //MARK: Long Press for the options
    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
        
        if sender.state == .began {
            let location: CGPoint = sender.location(in: sceneView)
            guard let result = sceneView.hitTest(location, options: nil).first else {
                return
            }
            print(result)
            let plane = SCNPlane.init(width: 0.3, height: 0.3)
            var node = SCNNode(geometry: plane)
            node = result.node
            let dict:NSMutableDictionary = NSMutableDictionary()
            let Id:Int? =  Int(node.name!)
            //            let resultArray:Array<Vertex> = appDelegate.vectorPointArray.arrVectorPoints.filter({$0.id == Id})
            //            print(resultArray)
            
            
            dict.setObject(node, forKey: "node" as NSCopying)
            dict.setObject(Id!, forKey: "node_id" as NSCopying)
            selectedNodeArray.add(dict)
            
            let alertC = UIAlertController(title: "Options", message: "", preferredStyle: .actionSheet)
            let addShelf = UIAlertAction(title: "Enter the name for the node", style: .default) { (alert) in
                self.addSection(dict: dict)
            }
            let deleteNode = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
                
            }
            //
            alertC.addAction(addShelf)
            alertC.addAction(deleteNode)
            self.present(alertC, animated: true, completion: nil)
        }
        else{
            let alertC = UIAlertController(title: "Long Press", message: "Long press gesture called when you press on view for 1 second.", preferredStyle: UIAlertControllerStyle.alert)
            let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alert) in
            }
            alertC.addAction(ok)
            self.present(alertC, animated: true, completion: nil)
        }
    }
    
    // MARK: Add sections to the nodes
    func addSection(dict: NSMutableDictionary){
        var nodeId:Int!
        sceneView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            if existingNode.name != nil{
                if dict["node_id"] as! Int == Int(existingNode.name!)!{
                    nodeId = dict["node_id"] as? Int
                }
            }
            
        }
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller: AddShelfViewController = storyboard.instantiateViewController(withIdentifier: "AddShelfViewController") as! AddShelfViewController
        controller.nodeId = nodeId
        controller.floorNumber = floorNumber
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: Remove particular node
    func removeNode(dict:NSMutableDictionary){
        
        sceneView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            if existingNode.name != nil{
                if dict["node_id"] as! Int == Int(existingNode.name!)!{
                    existingNode.removeFromParentNode()
                }
            }
            
        }
    }
    
    // MARK: Reset tracking configuration
    func resetTrackingConfiguration() {
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else { return }
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionImages = referenceImages
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.session.run(configuration, options: options)
    }
    
    // MARK: Touch Method for the Making adjacent Nodes
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isNode{  
            let touch:UITouch = touches.first!
            if(touch.view == self.sceneView){
                print("touch working")
                let viewTouchLocation:CGPoint = touch.location(in: sceneView)
                guard let result = sceneView.hitTest(viewTouchLocation, options: nil).first else {
                    return
                }
                //let sphereNode:SCNNode = sphereNode, sphereNode.contains(result.node)
                let plane = SCNPlane.init(width: 0.3, height: 0.3)
                var node = SCNNode(geometry: plane)
                node = result.node
                let dict:NSMutableDictionary = NSMutableDictionary()
                let Id:Int? =  Int(node.name!)
                dict.setObject(node, forKey: "node" as NSCopying)
                dict.setObject(Id!, forKey: "node_id" as NSCopying)
                selectedNodeArray.add(dict)
                if selectedNodeArray.count == 2{
                    for n in 0...appDelegate.vectorPointArray.arrVectorPoints.count - 1 {
                        let vertex: Vertex = appDelegate.vectorPointArray.arrVectorPoints[n] as Vertex
                        let dict:NSDictionary = selectedNodeArray[0] as! NSDictionary
                        if dict.value(forKey: "node_id") as? Int == vertex.id{
                            //let distance:Float = distanceBetweenTwoVectors(x1: vertex.vector.x, y1: vertex.vector.y, z1: vertex.vector.z, vectorPoints: )
                            let dictId:NSDictionary = selectedNodeArray[1] as! NSDictionary
                            let results = appDelegate.vectorPointArray.arrVectorPoints.filter{$0.id == (dictId.value(forKey: "node_id") as! Int)}
                            let distance:Float =  CommonMethods.sharedInstance.distanceBetweenTwoVectors(vectorPointsFrom: vertex.vector, vectorPointsTo: results[0].vector)
                            let dictAdd:NSMutableDictionary = NSMutableDictionary()
                            dictAdd.setObject(distance, forKey: "distance" as NSCopying)
                            dictAdd.setObject(dictId["node_id"] as! Int , forKey: "adjacent_node_id" as NSCopying)
                            vertex.arrAdjacent.add(dictAdd)
                            vertex.node = dict.value(forKey: "node") as! SCNNode
                        }
                        let dict1:NSDictionary = selectedNodeArray[1] as! NSDictionary
                        if dict1.value(forKey: "node_id") as? Int == vertex.id{
                            let dictId:NSDictionary = selectedNodeArray[0] as! NSDictionary
                            let results = appDelegate.vectorPointArray.arrVectorPoints.filter{$0.id == (dictId.value(forKey: "node_id") as! Int)}
                            let distance:Float = CommonMethods.sharedInstance.distanceBetweenTwoVectors(vectorPointsFrom: vertex.vector, vectorPointsTo: results[0].vector)
                            let dictAdd:NSMutableDictionary = NSMutableDictionary()
                            dictAdd.setObject(distance, forKey: "distance" as NSCopying)
                            dictAdd.setObject(dictId["node_id"] as! Int, forKey: "adjacent_node_id" as NSCopying)
                            vertex.arrAdjacent.add(dictAdd)
                            vertex.node = dict1.value(forKey: "node") as! SCNNode
                            self.AlertMessages(message: "Adjacent node added")
                        }
                    }
                    selectedNodeArray = NSMutableArray()
                }
                else if selectedNodeArray.count == 1{
                    let alertController:UIAlertController = UIAlertController.init(title: "", message: "Do you want to connect the tapped node to any adjacent node.", preferredStyle: .alert)
                    let alertOk:UIAlertAction = UIAlertAction.init(title: "YES", style: .default) { (Action:UIAlertAction) in
                        
                    }
                    let alertCancel:UIAlertAction = UIAlertAction.init(title: "NO", style: .cancel){ (Action:UIAlertAction) in
                        self.selectedNodeArray = nil
                    }
                    alertController.addAction(alertOk)
                    alertController.addAction(alertCancel)
                    self.present(alertController, animated: true, completion: nil)
                }
                for n in 0..<appDelegate.vectorPointArray.arrVectorPoints.count {
                    let vertex1:Vertex = appDelegate.vectorPointArray.arrVectorPoints[n] as Vertex
                    if vertex1.arrAdjacent.count > 2{
                        let node:SCNNode = vertex1.node
                        print(node)
                        node.geometry?.firstMaterial?.diffuse.contents = UIColor.red
                    }
                }
            }
        }
    }
    
    @IBAction func ButtonActionForNodes(_ sender: Any) {
        if isNode {
            self.sendPath(buttonSend)
        }
        else if isAttach{
            self.apiForNodeAttachment()
        }
    }
    // MARK: Api for attaching nodes to shelf
    func apiForNodeAttachment(){
        let arrResponse:NSMutableArray = NSMutableArray.init()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShelfMapping")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let dict:NSMutableDictionary = NSMutableDictionary.init()
                dict.setObject(data.value(forKey: "shelf_id") as! Int, forKey: "shelf_id" as NSCopying)
                dict.setObject(data.value(forKey: "node_id") as! Int, forKey: "node_id" as NSCopying)
                let is_added:Bool =  data.value(forKey: "is_added") as! Bool
                if !is_added{
                    arrResponse.add(dict)
                }
            }
        } catch {
            print("Failed")
        }
        
        var dict:[String:String] = [String:String]()
        dict["auth_token"] = UserDefaults.standard.value(forKey: "auth_token") as? String
        let idlibrary:Int =  UserDefaults.standard.value(forKey: "libraryId") as! Int

        var dictResponse:[String:Any] = [String:Any]()
        dictResponse["shelf_node"] = arrResponse
        dictResponse["floor"] = floorNumber
        dictResponse["library_id"] = idlibrary
        APICallExecutor.postRequestForURLString(true, "/admin/shelf_node", dict, dictResponse) { (Done, error, success, result, message, isActiveSession) in
            if success{
                DispatchQueue.main.async {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let context = appDelegate.persistentContainer.viewContext
                    //let entity = NSEntityDescription.entity(forEntityName: "ShelfMapping", in: context)
                    let ReqVar = NSFetchRequest<NSFetchRequestResult>(entityName: "ShelfMapping")
                    let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: ReqVar)
                    do{
                        try context.execute(DelAllReqVar)
                    }
                    catch{
                        print(error)
                        
                    }
                }
                for dict in arrResponse{
                    self.updateTableForShelf(dict: dict as! NSDictionary)
                }
                
                MyCustomClass.showAlert(Title: "", Message: "Shelf has been attach to Node Successfully", Controller: self, Time: 1)
            }
            else{
                DispatchQueue.main.async {
                    
                    for dict in arrResponse{
                        self.updateTableForShelf(dict: dict as! NSDictionary)
                    }
                }
                MyCustomClass.showAlert(Title: "", Message: "Please try again...", Controller: self, Time: 1)
                
            }
        }
        
    }
    
    func updateTableForShelf(dict:NSDictionary){
        
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "ShelfMapping", in: context)
            let newUser = NSManagedObject(entity: entity!, insertInto: context)
            let shelfId:Int = dict["shelf_id"] as! Int
            newUser.setValue(dict["shelf_id"], forKey: "node_id")
            newUser.setValue(shelfId, forKey: "shelf_id")
            newUser.setValue(true, forKey: "is_added")
            do {
                try context.save()
            } catch {
                print("Failed saving")
            }
        }
    }
    
    /*
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
     let context = appDelegate.persistentContainer.viewContext
     //                let entity = self.fetchedResultsController.fetchRequest.entity!
     
     let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"ShelfMapping")
     fetchRequest.predicate = NSPredicate(format: "shelf_id = %d",dict["shelf_id"] as! Int)
     
     do {
     let list = try context.fetch(fetchRequest) as? [NSManagedObject]
     if list!.count == 0 // Check notificationId available then not save
     {
     let newManagedObject = NSEntityDescription.insertNewObject(forEntityName: "ShelfMapping", into: context)
     newManagedObject.setValue(dict["node_id"], forKey: "node_id")
     newManagedObject.setValue(dict["shelf_id"], forKey: "shelf_id")
     newManagedObject.setValue(true, forKey: "is_added")
     
     }
     // success ...
     } catch let error as NSError {
     // failure
     print("Fetch failed: \(error.localizedDescription)")
     }*/
    // }
    
    
    // MARK: Add nodes to SceneView
    @IBAction func handleTap(sender: UITapGestureRecognizer) {
        
        if isScaned && isNode{
            let location = sender.location(in: sceneView)
            
            let hitTest = sceneView.hitTest(location, types: [ARHitTestResult.ResultType.featurePoint])
            
            guard let result = hitTest.last else { return }
            
            let transform = SCNMatrix4.init(result.worldTransform)
            let vector = SCNVector3Make(transform.m41, transform.m42, transform.m43)
            self.getVertex(vector: vector)
            
            let plane = SCNPlane.init(width: 0.3, height: 0.3)
            plane.cornerRadius = 0.15
            let material = SCNMaterial()
            plane.materials = [material]
            sphereNode = SCNNode(geometry: plane)
            sphereNode.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
            sphereNode.position = vector
            sphereNode.eulerAngles.x = -.pi / 2
            sceneView.anchor(for: sphereNode)
            sceneView.scene.rootNode.addChildNode(sphereNode)
            sphereNode.name = String(vertexId)
        }
        else{
            //self.getAlert(message: "Firstly scan the image then start making the map!!!")
            self.AlertMessages(message:"Firstly scan the image then start making the map!!" )
            print("Firstly scan the image then start making the map!!")
        }
    }
    
    func sortFunc(num1: Int, num2: Int) -> Bool {
        return num1 < num2
    }
    
    // MARK: Getting Vertex
    func getVertex(vector:SCNVector3){
        if appDelegate.vectorPointArray.arrVectorPoints.count > 0{
            let arrId:NSMutableArray = NSMutableArray()
            
            for n in appDelegate.vectorPointArray.arrVectorPoints{
                let dict :NSMutableDictionary = NSMutableDictionary()
                dict.setObject(n.id, forKey: "ID" as NSCopying)
                arrId.add(dict)
            }
            let sortedByDistance = arrId.sortedArray(using: [NSSortDescriptor(key: "ID", ascending: false)])
            let id:NSDictionary = sortedByDistance[0] as! NSDictionary
            vertexId = id.value(forKey: "ID") as! Int
            print(vertexId)
        }
        
        
        let vectorPoints = Vector.init()
        let vertex: Vertex = Vertex()
        let vectorDistance = VectorDistance.init()
        vectorId = vectorId + 1
        vertexId = vertexId + 1
        vertex.isAlreadyInserted = false
        vectorPoints.x = vector.x
        vectorPoints.y = vector.y
        vectorPoints.z = vector.z
        vectorPoints.id = vectorId
        var distance:Float = 0.0
        if lastNode != nil {
            
            distance = CommonMethods.sharedInstance.distanceBetweenTwoVectors(vectorPointsFrom: vectorPoints, vectorPointsTo: lastNode.vector)
            vectorDistance.distance = distance
            vectorDistance.vector = vectorPoints
            vertex.id = vertexId
            vertex.vector = vectorPoints
            appDelegate.vectorPointArray.arrVectorPoints.append(vertex)
            let count:NSInteger = appDelegate.vectorPointArray.arrVectorPoints.count
            
            var lastVertex:Vertex = Vertex.init()
            var currentVertex:Vertex = Vertex.init()
            lastVertex = appDelegate.vectorPointArray.arrVectorPoints[count - 2]
            currentVertex = appDelegate.vectorPointArray.arrVectorPoints[count - 1]
            let dictCurrent:NSMutableDictionary = NSMutableDictionary()
            dictCurrent.setObject(distance, forKey: "distance" as NSCopying)
            dictCurrent.setObject(currentVertex.id, forKey: "adjacent_node_id" as NSCopying)
            lastVertex.arrAdjacent.add(dictCurrent)
            let dictLast:NSMutableDictionary = NSMutableDictionary()
            dictLast.setObject(distance, forKey: "distance" as NSCopying)
            dictLast.setObject(lastVertex.id, forKey: "adjacent_node_id" as NSCopying)
            currentVertex.arrAdjacent.add(dictLast)
            print(lastVertex.arrAdjacent)
        }
        else{
            vertex.id = vertexId
            vertex.vector = vectorPoints
            appDelegate.vectorPointArray.arrVectorPoints.append(vertex)
        }
        
        // Assign Vertex to instanceVariable
        lastNode = Vertex.init()
        lastNode = vertex
        
    }
    
    // MARK: - ARSessionDelegate
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        guard let frame = session.currentFrame else { return }
        updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)
    }
    
    func session(_ session: ARSession, didRemove anchors: [ARAnchor]) {
        guard let frame = session.currentFrame else { return }
        updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        updateSessionInfoLabel(for: session.currentFrame!, trackingState: camera.trackingState)
    }
    
    // MARK: - ARSessionObserver
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay.
        sessionInfoLabel.text = "Session was interrupted"
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required.
        sessionInfoLabel.text = "Session interruption ended"
        resetTracking()
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user.
        sessionInfoLabel.text = "Session failed: \(error.localizedDescription)"
        resetTracking()
    }
    
    // MARK: - Private methods
    private func updateSessionInfoLabel(for frame: ARFrame, trackingState: ARCamera.TrackingState) {
        // Update the UI to provide feedback on the state of the AR experience.
        let message: String
        
        switch trackingState {
        case .normal where frame.anchors.isEmpty:
            // No planes detected; provide instructions for this app's AR interactions.
            message = "Move the device around to detect horizontal surfaces."
            
        case .notAvailable:
            message = "Tracking unavailable."
            // self.resetTracking()
            
        case .limited(.excessiveMotion):
            message = "Tracking limited - Move the device more slowly."
            //self.resetTracking()
            
        case .limited(.insufficientFeatures):
            message = "Tracking limited - Point the device at an area with visible surface detail, or improve lighting conditions."
            //self.resetTracking()
            
        case .limited(.initializing):
            message = "Initializing AR session."
            // self.resetTracking()
            
        default:
            // No feedback needed when tracking is normal and planes are visible.
            // (Nor when in unreachable limited-tracking states.)
            message = ""
        }
        
        sessionInfoLabel.text = message
        sessionInfoView.isHidden = message.isEmpty
    }
    
    //MARK: Options for path
    
    @IBAction func optionsForPath(_ sender: Any) {
        let alertController = UIAlertController.init(title: "Options", message: "", preferredStyle: .actionSheet)
        let action1:UIAlertAction = UIAlertAction.init(title: "Node Creation", style: .default) { (action:UIAlertAction) in
            self.nodeCreationTapped()
        }
        let action2:UIAlertAction = UIAlertAction.init(title: "Attach Node to Shelf", style: .default) { (action:UIAlertAction) in
            self.attachNodeToSelfTapped()
        }
        let action3:UIAlertAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        self.present(alertController, animated: true, completion: nil)
        
    }
    //MARK: Method for attach node for options
    func attachNodeToSelfTapped(){
        buttonSave.isHidden = false
        buttonSave.setTitle("Attach Nodes to Shelf", for: .normal)
        sceneView.addGestureRecognizer(longGesture)
        self.isAttach = true
        self.isNode = false
        tapRecognizer.removeTarget(self, action: #selector(handleTap))
    }
    
    //MARK: Method for node creation for options
    func nodeCreationTapped(){
        buttonSave.isHidden = false
        buttonSave.setTitle("Synch Nodes to Server", for: .normal)
        tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapRecognizer.numberOfTapsRequired = 1
        self.sceneView.removeGestureRecognizer(self.longGesture)
        self.isNode = true
        self.isAttach = false
    }
    
    //MARK: Back button action
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        self.sendPath(buttonSend)
        if appDelegate.arrayPoints.count > 0{
            
        }
        else{
            let alert = UIAlertController.init(title: "Alert", message: "Please map the floor then check for the path", preferredStyle: .alert)
            let Done: UIAlertAction = UIAlertAction.init(title:"Ok" , style: .cancel, handler: nil)
            alert.addAction(Done)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: Make Path
    func makePath(arr:Array<Node>,sceneView:ARSCNView){
        self.sceneView = sceneView
        self.sceneView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            existingNode.removeFromParentNode()
        }
        if arr.count > 0 {
            for n in 0..<arr.count {
                let dict:Node = arr[n]
                let x:Float = dict.x
                let y:Float = dict.y
                let z:Float = dict.z
                
                
                let plane = SCNPlane.init(width: 0.3, height: 0.3)
                plane.cornerRadius = 0.15
                let material = SCNMaterial()
                plane.materials = [material]
                sphereNode = SCNNode(geometry: plane)
                sphereNode.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
                sphereNode.position = SCNVector3(x, y, z)
                sphereNode.eulerAngles.x = -.pi / 2
                sceneView.anchor(for: sphereNode)
                sceneView.scene.rootNode.addChildNode(sphereNode)
            }
        }
        else{
            let alert = UIAlertController.init(title: "Alert", message: "Unable to find the path. Please try again !!!", preferredStyle: .alert)
            let Done: UIAlertAction = UIAlertAction.init(title:"Ok" , style: .cancel, handler: nil)
            alert.addAction(Done)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    // MARK: Reset Tracking
    private func resetTracking() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
    
    // MARK: Send Path to Server
    @IBAction func sendPath(_ sender: Any) {
        for n in 0..<appDelegate.vectorPointArray.arrVectorPoints.count{
            let vertex:Vertex = appDelegate.vectorPointArray.arrVectorPoints[n]
            let isAlreadyInserted:Bool = vertex.isAlreadyInserted
            if !isAlreadyInserted{
                let dictPoints:NSMutableDictionary = NSMutableDictionary.init()
                let x:Float = vertex.vector.x
                let y:Float = vertex.vector.y
                let z:Float = vertex.vector.z
                dictPoints.setValue(x, forKey: "x")
                dictPoints.setValue(y, forKey: "y")
                dictPoints.setValue(z, forKey: "z")
                dictPoints.setValue(vertex.id, forKey: "node_id")
                dictPoints.setValue(vertex.arrAdjacent, forKey: "adjacent_nodes")
                appDelegate.arrayPoints.add(dictPoints)
            }
        }
        
        
        appDelegate.spinnerColor = UIColor.black
        loaderView.showLoaderOnScreen(vc: self, view:self.sceneView)
        appDelegate.spinnerColor = UIColor.black
        loaderView.showLoaderOnScreen(vc: self, view:self.sceneView)
        print(appDelegate.arrayPoints)
        let dictResponse:NSMutableDictionary = NSMutableDictionary()
        let idlibrary:Int =  UserDefaults.standard.value(forKey: "libraryId") as! Int
        
        dictResponse.setObject(idlibrary, forKey: "library_id" as NSCopying)
        dictResponse.setObject(floorNumber, forKey: "floor" as NSCopying)
        dictResponse.setObject(appDelegate.arrayPoints, forKey: "node" as NSCopying)
        let urlString:String = URI_HTTP + "/create_node"
        webService.postApiMethod(urlString: urlString, params: dictResponse, Auth_token: "", User_Email: "")
        webService.delegate = self
    }
    
    func postApiMethodResponse(responseData: Data) {
        loaderView.hideLoader()
        let responseDict = MyCustomClass.checkJsonArrayDictionaryString(data: responseData) as! NSDictionary
        
        let success = responseDict["success"] as! Bool
        if success{
            MyCustomClass.showAlert(Title: "", Message: "Nodes synchronized successfully", Controller: self, Time: 1)
        }
        else{
            MyCustomClass.showAlert(Title: "", Message: "Nodes were not synchronized", Controller: self, Time: 1)
            
        }
        print(responseDict)
    }
    func gettingFail(error: String) {
        loaderView.hideLoader()
        loaderView.hideLoader()
    }
    
    // MARK: Get Nodes from the server
    func getNodes() {
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            appDelegate.spinnerColor = UIColor.black
            loaderView.showLoaderOnScreen(vc: self, view:self.sceneView)
            let idlibrary:Int =  UserDefaults.standard.value(forKey: "libraryId") as! Int
            var dict: [String:Any] = [String:Any]()
            dict["library_id"] = idlibrary
            dict["floor"] = floorNumber
            var dictHeader:[String:String] = [String:String]()
            dictHeader["auth_token"] = UserDefaults.standard.value(forKey: "auth_token") as? String 
            
            let urlString:String = "/node_list"
            
            APICallExecutor.postRequestForURLString(true,urlString , dictHeader, dict) { (Done, error, success, result, message, isActiveSession) in
                self.loaderView.hideLoader()
                
                if success{
                    let dict:NSDictionary = result as! NSDictionary
                    let arrPoints:NSMutableArray = dict.value(forKey:"nodes") as! NSMutableArray
                    if  arrPoints.count > 0{
                        for dict in arrPoints{
                            print(dict)
                            let dictNode:NSDictionary = dict as! NSDictionary
                            let x:Double = dictNode["x"] as! Double
                            let y:Double = dictNode["y"] as! Double
                            let z:Double = dictNode["z"] as! Double
                            let nodeID:Int = dictNode["node_id"] as! Int
                            let arrAdjacentNode:NSMutableArray = dictNode["adjacent_nodes"] as! NSMutableArray
                            let vectorPoints = Vector.init()
                            vectorPoints.x = Float(x)
                            vectorPoints.y = Float(y)
                            vectorPoints.z = Float(z)
                            let vertex: Vertex = Vertex()
                            vertex.id = nodeID
                            vertex.vector = vectorPoints
                            vertex.isAlreadyInserted = true
                            if arrAdjacentNode.count > 0{
                                for dictAdjacent in arrAdjacentNode{
                                    let dictAdj:NSDictionary = dictAdjacent as! NSDictionary
                                    let adjacentNodeID:Int = dictAdj["adjacent_node_id"] as! Int
                                    let distance:Double = dictAdj["distance_from_route_node"] as! Double
                                    
                                    let dictAdd:NSMutableDictionary = NSMutableDictionary.init()
                                    dictAdd.setObject(adjacentNodeID, forKey: "distance" as NSCopying)
                                    dictAdd.setObject(distance, forKey: "adjacent_node_id" as NSCopying)
                                    vertex.arrAdjacent.add(dictAdd)
                                }
                            }
                            //vertexArrayServer.arrVectorPoints.append(vertex)
                            self.appDelegate.vectorPointArray.arrVectorPoints.append(vertex)
                            
                        }
                        self.makeExistingNodes(arr: self.appDelegate.vectorPointArray.arrVectorPoints)
                        self.getLastNode()
                    }
                    
                    //  print(dict)fghfghdfg
                }
                else{
                    print(message as Any)
                }
            }
        }
        else{
            MyCustomClass.showAlert(Title: "Alert", Message: "No internet connection", Controller: self, Time: 3)
        }
    }
    
    
    func getLastNode(){
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            appDelegate.spinnerColor = UIColor.black
            loaderView.showLoaderOnScreen(vc: self, view:self.sceneView)
            let idlibrary:Int =  UserDefaults.standard.value(forKey: "libraryId") as! Int
            var dict: [String:Any] = [String:Any]()
            dict["library_id"] = idlibrary
            dict["floor"] = 1
            
            var dictHeader:[String:String] = [String:String]()
            dictHeader["auth_token"] = UserDefaults.standard.value(forKey: "auth_token") as? String
            
            let urlString:String = "/check_node_id"
            
            APICallExecutor.postRequestForURLString(true,urlString , dictHeader, dict) { (Done, error, success, result, message, isActiveSession) in
                self.loaderView.hideLoader()
                if success{
                    let dict:NSDictionary = result as! NSDictionary
                    print(dict)
                    let nodeId:Int = dict["node_id"] as! Int
                    self.vertexId = nodeId + 1
                }
            }
        }
    }
    func makeExistingNodes(arr:Array<Vertex>){
        for n in arr {
            let dict:Vertex = n
            let x:Float = dict.vector.x
            let y:Float = dict.vector.y
            let z:Float = dict.vector.z
            
            
            let plane = SCNPlane.init(width: 0.3, height: 0.3)
            plane.cornerRadius = 0.15
            let material = SCNMaterial()
            plane.materials = [material]
            sphereNode = SCNNode(geometry: plane)
            sphereNode.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
            sphereNode.position = SCNVector3(x, y, z)
            sphereNode.eulerAngles.x = -.pi / 2
            sceneView.anchor(for: sphereNode)
            sphereNode.name = String(dict.id)
            n.node = sphereNode
            sceneView.scene.rootNode.addChildNode(sphereNode)
            
        }
    }
    //MARK: ACNACtion for imageHighlight
    var imageHighlightAction: SCNAction {
        return .sequence([
            .wait(duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            .fadeOpacity(to: 0.15, duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            .fadeOut(duration: 0.5),
            .removeFromParentNode()
            ])
    }
}


extension ViewController: ARSCNViewDelegate {
    
    //    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    //        DispatchQueue.main.async {
    //            guard let imageAnchor = anchor as? ARImageAnchor,
    //                let imageName = imageAnchor.referenceImage.name else { return }
    //            //self.getNode(withImageName: imageName)
    //            self.getNode(withImageName: imageName)
    //        }
    //    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if !isScanned{
            guard let imageAnchor = anchor as? ARImageAnchor else { return }
            let referenceImage = imageAnchor.referenceImage
            updateQueue.async {
                // Add the plane visualization to the scene.
                // let imageTransform = imageAnchor.transform
                let cameraTransform = self.sceneView.session.currentFrame?.camera.transform
                
                let anchorPosition = imageAnchor.transform.columns.3
                let cameraPosition = cameraTransform!.columns.3
                
                // here’s a line connecting the two points, which might be useful for other things
                let cameraToAnchor = cameraPosition - anchorPosition
                // and here’s just the scalar distance
                let distance = length(cameraToAnchor)
                
                //let distance:Float = SCNVector3.distanceFrom(vector: planeNode.worldPosition, toVector: cameraVector)
                if distance > 0.40 && distance < 0.50{
                    let imageName = imageAnchor.referenceImage.name
                    self.getNode(withImageName: imageName!)
                    // Create a plane to visualize the initial position of the detected image.
                    let plane = SCNPlane(width: referenceImage.physicalSize.width,
                                         height: referenceImage.physicalSize.height)
                    let planeNode = SCNNode(geometry: plane)
                    planeNode.opacity = 0.25
                    /*
                     `SCNPlane` is vertically oriented in its local coordinate space, but
                     `ARImageAnchor` assumes the image is horizontal in its local space, so
                     rotate the plane to match.
                     */
                    planeNode.eulerAngles.x = -.pi / 2
                    
                    /*
                     Image anchors are not tracked after initial detection, so create an
                     animation that limits the duration for which the plane visualization appears.
                     */
                    planeNode.runAction(self.imageHighlightAction)
                    node.addChildNode(planeNode)
                    self.viewBox.isHidden = true
                }
                else if distance < 0.40{
                    DispatchQueue.main.async {
                        self.instructionLabel.text = "Please move the phone away from image."
                    }
                }
                else if distance > 0.50{
                    DispatchQueue.main.async {
                        self.instructionLabel.text = "Please move the phone towards image."
                    }
                }
            }
        }
    }
    func dis(lhv:SCNVector3, rhv:SCNVector3) -> SCNVector3 {
        return SCNVector3(lhv.x + rhv.x, lhv.y + rhv.y, lhv.z + rhv.z)
    }
    func getPlaneNode(withReferenceImage image: ARReferenceImage) -> SCNNode {
        let plane = SCNPlane(width: image.physicalSize.width,
                             height: image.physicalSize.height)
        let node = SCNNode(geometry: plane)
        return node
    }
    
    func getNode(withImageName name: String) -> SCNNode {
        let node = SCNNode()
        isScaned = true
        
        switch name {
        case "Book":
            DispatchQueue.main.async {
                self.instructionLabel.isHidden = true
                self.viewBox.isHidden = true
            }
            floorNumber = 1
            self.resetTracking()
            getAlert(message: "Session Initiated")
            break
            
        case "Snow Mountain":
            DispatchQueue.main.async {
                self.instructionLabel.isHidden = true
                self.viewBox.isHidden = true
            }
            floorNumber = 2
            self.resetTracking()
            getAlert(message: "Session Initiated")
            break
            
        case "Trees In the Dark":
            DispatchQueue.main.async {
                self.instructionLabel.isHidden = true
                self.viewBox.isHidden = true
            }
            floorNumber = 3
            self.resetTracking()
            getAlert(message: "Session Initiated")
            break
            
        default:
            break
        }
        
        return node
    }
    
    func getAlert(message:String){
        let alertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        let alert = UIAlertAction.init(title: "OK", style: .default) { (action:UIAlertAction) in
            self.getNodes()
        }
        alertController.addAction(alert)
        self.present(alertController, animated: true, completion: nil)
        
    }
    func AlertMessages(message:String){
        let alertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        let alert = UIAlertAction.init(title: "OK", style: .cancel) { (action:UIAlertAction) in
        }
        alertController.addAction(alert)
        self.present(alertController, animated: true, completion: nil)
        
    }
}

//MARK: Extension of the UIView
@IBDesignable extension UIView {
    //MARK: Method for the border width
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    //MARK: Method for the corner radius
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    //MARK: Method for border color
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
