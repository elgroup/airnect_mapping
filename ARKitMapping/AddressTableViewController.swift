//
//  AddressTableViewController.swift
//  AirnectFloorMapping
//
//  Created by Anvesh on 07/08/18.
//  Copyright © 2018 Anvesh. All rights reserved.
//

import UIKit
import  CoreLocation


class AddressTableViewController: UITableViewController,UITextFieldDelegate,ShowAddressDelegate,UIPickerViewDataSource,UIPickerViewDelegate {
    
    @IBOutlet var textFieldLibraryName: UITextField!
    @IBOutlet var textFieldAddressLine1: UITextField!
    @IBOutlet var textFieldAddressLine2: UITextField!
    @IBOutlet var textFieldCity: UITextField!
    @IBOutlet var textFieldState: UITextField!
    @IBOutlet var textFieldCountry: UITextField!
    @IBOutlet var textFieldZipCode: UITextField!
    @IBOutlet var buttonSubmit: UIButton!
    var intStateID:Int!
    var intCountryID:Int!
    var textFieldSelected:UITextField!
    var CountryList:NSArray = NSArray()
    var StateList:NSArray = NSArray()
    var pickerView:UIPickerView!
    var dictUser:NSDictionary!
    var intSelectedIndex:Int = 0
    var dictLibrary:NSDictionary!
    var lat:Double!
    var lon:Double!
    var isEditng = false
    var libraryId:Int!
    
    var loaderView: Loader = Loader()
    var webService: WebServiceSessionHelper = WebServiceSessionHelper()
    var fetchlocation:FetchLocation = FetchLocation.init()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchlocation.delegate = self
        print(dictUser)
        print(intSelectedIndex)
        dictLibrary = NSDictionary()
        let array:NSArray = dictUser.value(forKey: "library_list") as! NSArray
        dictLibrary = array.object(at: intSelectedIndex) as! NSDictionary
        libraryId = dictLibrary.value(forKey: "id") as! Int
        buttonSubmit.isHidden = true
        pickerView = UIPickerView.init(frame: CGRect(x: 0, y: 250, width: UIScreen.main.bounds.size.width, height: 250))
        pickerView.delegate = self
        textFieldState.inputView = pickerView
        textFieldCountry.inputView = pickerView
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"back"), style: .plain, target: self, action: #selector(backTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        let rightEditBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"edit"), style: .plain, target: self, action:  #selector(editTapped(_:)))
        rightEditBarButtonItem.tintColor = UIColor.white
        // let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let rightNextBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"right-arrow"), style: .plain, target: self, action:  #selector(nextTapped))
        rightNextBarButtonItem.tintColor = UIColor.white
        self.navigationItem.setRightBarButtonItems([rightNextBarButtonItem,rightEditBarButtonItem], animated: true)
        self.fillTheFields()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func nextTapped(){
        let defaults = UserDefaults.standard
        defaults.set(libraryId!, forKey: "libraryId")
        let story:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller:ViewController = story.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func editTapped( _ button:UIBarButtonItem){
        
        textFieldZipCode.isUserInteractionEnabled = true
        textFieldState.isUserInteractionEnabled = true
        textFieldCountry.isUserInteractionEnabled = true
        textFieldCity.isUserInteractionEnabled = true
        textFieldLibraryName.isUserInteractionEnabled = true
        textFieldAddressLine1.isUserInteractionEnabled = true
        textFieldAddressLine2.isUserInteractionEnabled = true
        buttonSubmit.isHidden = false
    }
    
    func fillTheFields(){
        textFieldLibraryName.text = dictLibrary.value(forKey: "name") as? String
        textFieldCity.text = dictLibrary.value(forKey: "city") as? String
        textFieldAddressLine1.text = dictLibrary.value(forKey: "address_line_1") as? String
        textFieldAddressLine2.text = dictLibrary.value(forKey: "address_line_2") as? String
        intStateID = dictLibrary.value(forKey: "state_id") as! Int
        intCountryID = dictLibrary.value(forKey: "country_id") as! Int
        textFieldCountry.text = self.getCountryName(countryId: intCountryID)
        textFieldState.text = self.getStateName(stateId: intStateID, countryId: intCountryID)
        let intZipCode:Int = dictLibrary.value(forKey: "zip_code") as! Int
        textFieldZipCode.text = "\(intZipCode)"
    }
    
    func getCountryName(countryId:Int) -> String{
        var dict:NSDictionary = NSDictionary()
        if let path = Bundle.main.path(forResource: "Country", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    // do stuff
                    CountryList = jsonResult["country"] as! NSArray
                    let resultPredicate = NSPredicate(format: "id = %d", countryId)
                    let searchResults:NSArray = CountryList.filtered(using: resultPredicate) as NSArray
                    dict = searchResults.object(at: 0) as! NSDictionary
                }
            } catch {
            }
        }
        return dict.value(forKey: "name") as! String
    }
    
    func getStateName(stateId:Int,countryId:Int) -> String{
        var dict:NSDictionary = NSDictionary()
        if let path = Bundle.main.path(forResource: "State", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    // do stuff
                    StateList = jsonResult["state"] as! NSArray
                    let resultPredicate = NSPredicate(format: "id = %d AND country_id = %d", stateId,countryId )
                    let searchResults:NSArray = StateList.filtered(using: resultPredicate) as NSArray
                    dict = searchResults.object(at: 0) as! NSDictionary
                }
            } catch {
            }
        }
        return dict.value(forKey: "name") as! String
    }
    
    //MARK: UIPickerView Delegate and DataSource
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if textFieldSelected == textFieldCountry{
            return CountryList.count
        }
        else{
            let resultPredicate = NSPredicate(format: "country_id = %d", intCountryID )
            let searchResults:NSArray = StateList.filtered(using: resultPredicate) as NSArray
            return searchResults.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if textFieldSelected == textFieldCountry{
            let dictCountryName:NSDictionary = CountryList.object(at: row) as! NSDictionary
            let strCountryName:String = dictCountryName.value(forKey: "name") as! String
            intCountryID = dictCountryName.value(forKey: "id") as! Int
            return strCountryName
        }
        else{
            let resultPredicate = NSPredicate(format: "country_id = %d", intCountryID )
            let searchResults:NSArray = StateList.filtered(using: resultPredicate) as NSArray
            let dictStateList:NSDictionary = searchResults.object(at: row) as! NSDictionary
            let strStateName:String = dictStateList.value(forKey: "name") as! String
            return strStateName
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textFieldSelected == textFieldCountry{
            let dictCountryName:NSDictionary = CountryList.object(at: row) as! NSDictionary
            let strCountryName:String = dictCountryName.value(forKey: "name") as! String
            intCountryID = dictCountryName.value(forKey: "id") as! Int
            textFieldCountry.text = strCountryName
        }
        else{
            let resultPredicate = NSPredicate(format: "country_id = %d", intCountryID )
            let searchResults:NSArray = StateList.filtered(using: resultPredicate) as NSArray
            let dictStateList:NSDictionary = searchResults.object(at: row) as! NSDictionary
            let strStateName:String = dictStateList.value(forKey: "name") as! String
            intStateID = dictStateList.value(forKey: "id") as! Int
            textFieldState.text = strStateName
        }
    }
    
    func setAddress(placemark: CLPlacemark, latitude: Double, longitude: Double) {
        
        print(placemark.country!)
        //        if placemark.name != nil{
        //            textFieldLibraryName.text = "\(placemark.name!)"
        //        }
        //        if placemark.subLocality != nil {
        //            textFieldAddressLine1.text = "\(placemark.subLocality!)"
        //        }
        //        if placemark.thoroughfare != nil {
        //            textFieldAddressLine2.text = "\(placemark.thoroughfare!)"
        //        }
        //        if placemark.locality != nil {
        //            textFieldCity.text = "\(placemark.locality!)"
        //        }
        //        if placemark.country != nil {
        //            textFieldCountry.text = "\(placemark.country!)"
        //        }
        //        if placemark.postalCode != nil {
        //            textFieldZipCode.text = "\(placemark.postalCode!)"
        //        }
        //        if placemark.administrativeArea != nil{
        //            textFieldState.text = "\(placemark.administrativeArea!)"
        //        }
        //        lat = latitude
        //        lon = longitude
    }
    
    @IBAction func buttonSubmitPressed(_ sender: Any) {
        if (textFieldLibraryName.text?.count)! > 0 {
            self.checkForAddressCompletion()
        }
        else{
            let alertController = UIAlertController.init(title: "", message: "Library name can't be blank", preferredStyle: .alert)
            let alert = UIAlertAction.init(title: "OK", style: .default) { (action:UIAlertAction) in
            }
            alertController.addAction(alert)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    // MARK: TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldLibraryName{
            textField.becomeFirstResponder()
        }
        else if textField == textFieldAddressLine1{
            textFieldLibraryName.resignFirstResponder()
            textField.becomeFirstResponder()
        }
        else if textField == textFieldAddressLine2{
            textFieldAddressLine1.resignFirstResponder()
            textField.becomeFirstResponder()
        }
        else if textField == textFieldCountry{
            textFieldAddressLine2.resignFirstResponder()
            textField.becomeFirstResponder()
            textFieldSelected = textFieldCountry
            pickerView.reloadAllComponents()
        }
        else if textField == textFieldState{
            textFieldCountry.resignFirstResponder()
            textField.becomeFirstResponder()
            textFieldSelected = textFieldState
            pickerView.reloadAllComponents()
            
        }
        else if textField == textFieldCity{
            textFieldState.resignFirstResponder()
            textField.becomeFirstResponder()
            
        }
        else if textField == textFieldZipCode{
            textFieldCountry.resignFirstResponder()
            textField.becomeFirstResponder()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldLibraryName{
            textField.resignFirstResponder()
            textFieldAddressLine1.becomeFirstResponder()
        }
        else if textField == textFieldAddressLine1{
            textField.resignFirstResponder()
            textFieldAddressLine2.becomeFirstResponder()
        }
        else if textField == textFieldAddressLine2{
            textFieldAddressLine2.resignFirstResponder()
            textFieldCountry.becomeFirstResponder()
            textFieldSelected = textFieldCountry
        }
        else if textField == textFieldCountry{
            textField.resignFirstResponder()
            textFieldState.becomeFirstResponder()
            textFieldSelected = textFieldState
        }
        else if textField == textFieldState{
            textField.resignFirstResponder()
            textFieldCity.becomeFirstResponder()
        }
        else if textField == textFieldCity{
            textField.resignFirstResponder()
            textFieldZipCode.becomeFirstResponder()
        }
        else if textField == textFieldZipCode{
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK: Send Address to the server
    
    func checkForAddressCompletion(){
        if textFieldLibraryName.text?.count == 0{
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter the Library Name", Controller: self, Time: 3)
        }
        else if textFieldAddressLine1.text?.count == 0{
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter Address", Controller: self, Time: 3)
        }
        else if textFieldCity.text?.count == 0{
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter City name", Controller: self, Time: 3)
        }
        else if textFieldState.text?.count == 0{
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter State name", Controller: self, Time: 3)
        }
        else if textFieldCountry.text?.count == 0 {
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter Country name", Controller: self, Time: 3)
        }
        else if textFieldZipCode.text?.count == 0{
            MyCustomClass.showAlert(Title: "Alert", Message: "Enter Zipcode name", Controller: self, Time: 3)
        }
        else{
            self.sendAddressToServer()
        }
    }
    
    func sendAddressToServer(){
        
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            loaderView.showLoaderOnScreen(vc: self, view:self.view)
            var dict: [String:Any] = [String:Any]()
            dict["name"] = textFieldLibraryName.text
            dict["address_line_1"] = textFieldAddressLine1.text
            dict["address_line_2"] = textFieldAddressLine2.text
            dict["city"] = textFieldCity.text
            dict["state_id"] = intStateID
            dict["country_id"] = intCountryID
            dict["zip_code"] = textFieldZipCode.text
            dict["latitude"] = lat
            dict["longitude"] = lon
            
            var dictResponse:[String:Any] = [String:Any]()
            dictResponse["library"] = dict
            var dictHeader:[String:String] = [String:String]()
            dictHeader["auth_token"] = UserDefaults.standard.value(forKey: "auth_token") as? String 
            let urlString:String = "/libraries/\(libraryId!)"
            
            APICallExecutor.putRequestForURLString(true, urlString, dictHeader, dictResponse) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async{
                    self.loaderView.hideLoader()
                    if Success{
                        
                        let dict:NSDictionary = result as! NSDictionary
                        let dictlibrary:NSDictionary = dict["library"] as! NSDictionary
                        let id:Int = dictlibrary["id"] as! Int
                        let defaults = UserDefaults.standard
                        defaults.set(id, forKey: "libraryId")
                        let idlibrary:Int =  UserDefaults.standard.value(forKey: "libraryId") as! Int
                        print(idlibrary)
                        let story:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let controller:ViewController = story.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                    else{
                        print(message)
                    }
                }
            }
        }
        else{
            MyCustomClass.showAlert(Title: "Alert", Message: "No internet connection", Controller: self, Time: 3)
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9    
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
}
